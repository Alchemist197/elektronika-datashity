(function(){var k,aa="function"==typeof Object.create?Object.create:function(a){function b(){}
b.prototype=a;return new b},ba;
if("function"==typeof Object.setPrototypeOf)ba=Object.setPrototypeOf;else{var ca;a:{var da={Sa:!0},ea={};try{ea.__proto__=da;ca=ea.Sa;break a}catch(a){}ca=!1}ba=ca?function(a,b){a.__proto__=b;if(a.__proto__!==b)throw new TypeError(a+" is not extensible");return a}:null}
var fa=ba,ha="function"==typeof Object.defineProperties?Object.defineProperty:function(a,b,c){a!=Array.prototype&&a!=Object.prototype&&(a[b]=c.value)},ia="undefined"!=typeof window&&window===this?this:"undefined"!=typeof global&&null!=global?global:this;
function ja(a){if(a){for(var b=ia,c=["Reflect","construct"],d=0;d<c.length-1;d++){var e=c[d];e in b||(b[e]={});b=b[e]}c=c[c.length-1];d=b[c];a=a(d);a!=d&&null!=a&&ha(b,c,{configurable:!0,writable:!0,value:a})}}
var ka=function(){function a(){function a(){}
Reflect.construct(a,[],function(){});
return new a instanceof a}
if("undefined"!=typeof Reflect&&Reflect.construct){if(a())return Reflect.construct;var b=Reflect.construct;return function(a,d,e){a=b(a,d);e&&Reflect.setPrototypeOf(a,e.prototype);return a}}return function(a,b,e){void 0===e&&(e=a);
e=aa(e.prototype||Object.prototype);return Function.prototype.apply.call(a,e,b)||e}}();
ja(function(){return ka});
var m=this;function la(a){return void 0!==a}
function p(a){return"string"==typeof a}
function ma(a){return"number"==typeof a}
function q(a){a=a.split(".");for(var b=m,c=0;c<a.length;c++)if(b=b[a[c]],null==b)return null;return b}
function oa(){}
function pa(a){a.U=void 0;a.m=function(){return a.U?a.U:a.U=new a}}
function qa(a){var b=typeof a;if("object"==b)if(a){if(a instanceof Array)return"array";if(a instanceof Object)return b;var c=Object.prototype.toString.call(a);if("[object Window]"==c)return"object";if("[object Array]"==c||"number"==typeof a.length&&"undefined"!=typeof a.splice&&"undefined"!=typeof a.propertyIsEnumerable&&!a.propertyIsEnumerable("splice"))return"array";if("[object Function]"==c||"undefined"!=typeof a.call&&"undefined"!=typeof a.propertyIsEnumerable&&!a.propertyIsEnumerable("call"))return"function"}else return"null";
else if("function"==b&&"undefined"==typeof a.call)return"object";return b}
function ra(a){var b=qa(a);return"array"==b||"object"==b&&"number"==typeof a.length}
function sa(a){return"function"==qa(a)}
function ta(a){var b=typeof a;return"object"==b&&null!=a||"function"==b}
function ua(a){return a[va]||(a[va]=++wa)}
var va="closure_uid_"+(1E9*Math.random()>>>0),wa=0;function xa(a,b,c){return a.call.apply(a.bind,arguments)}
function ya(a,b,c){if(!a)throw Error();if(2<arguments.length){var d=Array.prototype.slice.call(arguments,2);return function(){var c=Array.prototype.slice.call(arguments);Array.prototype.unshift.apply(c,d);return a.apply(b,c)}}return function(){return a.apply(b,arguments)}}
function r(a,b,c){Function.prototype.bind&&-1!=Function.prototype.bind.toString().indexOf("native code")?r=xa:r=ya;return r.apply(null,arguments)}
var za=Date.now||function(){return+new Date};
function t(a,b){var c=a.split("."),d=m;c[0]in d||!d.execScript||d.execScript("var "+c[0]);for(var e;c.length&&(e=c.shift());)!c.length&&la(b)?d[e]=b:d[e]&&d[e]!==Object.prototype[e]?d=d[e]:d=d[e]={}}
function u(a,b){function c(){}
c.prototype=b.prototype;a.u=b.prototype;a.prototype=new c;a.prototype.constructor=a;a.Gb=function(a,c,f){for(var d=Array(arguments.length-2),e=2;e<arguments.length;e++)d[e-2]=arguments[e];return b.prototype[c].apply(a,d)}}
;function Aa(a){if(Error.captureStackTrace)Error.captureStackTrace(this,Aa);else{var b=Error().stack;b&&(this.stack=b)}a&&(this.message=String(a))}
u(Aa,Error);Aa.prototype.name="CustomError";var Ba;var Ca=Array.prototype.indexOf?function(a,b){return Array.prototype.indexOf.call(a,b,void 0)}:function(a,b){if(p(a))return p(b)&&1==b.length?a.indexOf(b,0):-1;
for(var c=0;c<a.length;c++)if(c in a&&a[c]===b)return c;return-1},x=Array.prototype.forEach?function(a,b,c){Array.prototype.forEach.call(a,b,c)}:function(a,b,c){for(var d=a.length,e=p(a)?a.split(""):a,f=0;f<d;f++)f in e&&b.call(c,e[f],f,a)},Da=Array.prototype.filter?function(a,b,c){return Array.prototype.filter.call(a,b,c)}:function(a,b,c){for(var d=a.length,e=[],f=0,g=p(a)?a.split(""):a,h=0;h<d;h++)if(h in g){var l=g[h];
b.call(c,l,h,a)&&(e[f++]=l)}return e},Ea=Array.prototype.map?function(a,b){return Array.prototype.map.call(a,b,void 0)}:function(a,b){for(var c=a.length,d=Array(c),e=p(a)?a.split(""):a,f=0;f<c;f++)f in e&&(d[f]=b.call(void 0,e[f],f,a));
return d},Fa=Array.prototype.some?function(a,b){return Array.prototype.some.call(a,b,void 0)}:function(a,b){for(var c=a.length,d=p(a)?a.split(""):a,e=0;e<c;e++)if(e in d&&b.call(void 0,d[e],e,a))return!0;
return!1};
function Ga(a,b){a:{var c=a.length;for(var d=p(a)?a.split(""):a,e=0;e<c;e++)if(e in d&&b.call(void 0,d[e],e,a)){c=e;break a}c=-1}return 0>c?null:p(a)?a.charAt(c):a[c]}
function Ha(a,b){return 0<=Ca(a,b)}
function Ia(a){return Array.prototype.concat.apply([],arguments)}
function Ja(a){var b=a.length;if(0<b){for(var c=Array(b),d=0;d<b;d++)c[d]=a[d];return c}return[]}
function Ka(a,b){for(var c=1;c<arguments.length;c++){var d=arguments[c];if(ra(d)){var e=a.length||0,f=d.length||0;a.length=e+f;for(var g=0;g<f;g++)a[e+g]=d[g]}else a.push(d)}}
function La(a,b,c,d){return Array.prototype.splice.apply(a,Ma(arguments,1))}
function Ma(a,b,c){return 2>=arguments.length?Array.prototype.slice.call(a,b):Array.prototype.slice.call(a,b,c)}
function Na(a){for(var b=[],c=0;c<arguments.length;c++){var d=arguments[c];if("array"==qa(d))for(var e=0;e<d.length;e+=8192)for(var f=Na.apply(null,Ma(d,e,e+8192)),g=0;g<f.length;g++)b.push(f[g]);else b.push(d)}return b}
;var Oa=String.prototype.trim?function(a){return a.trim()}:function(a){return/^[\s\xa0]*([\s\S]*?)[\s\xa0]*$/.exec(a)[1]},Pa=String.prototype.repeat?function(a,b){return a.repeat(b)}:function(a,b){return Array(b+1).join(a)};
function Qa(a){a=la(void 0)?a.toFixed(void 0):String(a);var b=a.indexOf(".");-1==b&&(b=a.length);return Pa("0",Math.max(0,2-b))+a}
function Ra(a,b){for(var c=0,d=Oa(String(a)).split("."),e=Oa(String(b)).split("."),f=Math.max(d.length,e.length),g=0;0==c&&g<f;g++){var h=d[g]||"",l=e[g]||"";do{h=/(\d*)(\D*)(.*)/.exec(h)||["","","",""];l=/(\d*)(\D*)(.*)/.exec(l)||["","","",""];if(0==h[0].length&&0==l[0].length)break;c=Sa(0==h[1].length?0:parseInt(h[1],10),0==l[1].length?0:parseInt(l[1],10))||Sa(0==h[2].length,0==l[2].length)||Sa(h[2],l[2]);h=h[3];l=l[3]}while(0==c)}return c}
function Sa(a,b){return a<b?-1:a>b?1:0}
function Ta(a){return String(a).replace(/\-([a-z])/g,function(a,c){return c.toUpperCase()})}
function Ua(a){var b=p(void 0)?"undefined".replace(/([-()\[\]{}+?*.$\^|,:#<!\\])/g,"\\$1").replace(/\x08/g,"\\x08"):"\\s";return a.replace(new RegExp("(^"+(b?"|["+b+"]+":"")+")([a-z])","g"),function(a,b,e){return b+e.toUpperCase()})}
;function Va(a,b){this.h=a;this.i=b;this.f=0;this.b=null}
Va.prototype.get=function(){if(0<this.f){this.f--;var a=this.b;this.b=a.next;a.next=null}else a=this.h();return a};
function Wa(a,b){a.i(b);100>a.f&&(a.f++,b.next=a.b,a.b=b)}
;var y;a:{var Xa=m.navigator;if(Xa){var Ya=Xa.userAgent;if(Ya){y=Ya;break a}}y=""}function z(a){return-1!=y.indexOf(a)}
;function Za(a,b,c){for(var d in a)b.call(c,a[d],d,a)}
function $a(a){var b=[],c=0,d;for(d in a)b[c++]=a[d];return b}
function ab(a){var b=bb,c;for(c in b)if(a.call(void 0,b[c],c,b))return c}
var cb="constructor hasOwnProperty isPrototypeOf propertyIsEnumerable toLocaleString toString valueOf".split(" ");function db(a,b){for(var c,d,e=1;e<arguments.length;e++){d=arguments[e];for(c in d)a[c]=d[c];for(var f=0;f<cb.length;f++)c=cb[f],Object.prototype.hasOwnProperty.call(d,c)&&(a[c]=d[c])}}
;function eb(){return z("Safari")&&!(fb()||z("Coast")||z("Opera")||z("Edge")||z("Silk")||z("Android"))}
function fb(){return(z("Chrome")||z("CriOS"))&&!z("Edge")}
function gb(){return z("Android")&&!(fb()||z("Firefox")||z("Opera")||z("Silk"))}
;function hb(a){m.setTimeout(function(){throw a;},0)}
var ib;
function jb(){var a=m.MessageChannel;"undefined"===typeof a&&"undefined"!==typeof window&&window.postMessage&&window.addEventListener&&!z("Presto")&&(a=function(){var a=document.createElement("IFRAME");a.style.display="none";a.src="";document.documentElement.appendChild(a);var b=a.contentWindow;a=b.document;a.open();a.write("");a.close();var c="callImmediate"+Math.random(),d="file:"==b.location.protocol?"*":b.location.protocol+"//"+b.location.host;a=r(function(a){if(("*"==d||a.origin==d)&&a.data==
c)this.port1.onmessage()},this);
b.addEventListener("message",a,!1);this.port1={};this.port2={postMessage:function(){b.postMessage(c,d)}}});
if("undefined"!==typeof a&&!z("Trident")&&!z("MSIE")){var b=new a,c={},d=c;b.port1.onmessage=function(){if(la(c.next)){c=c.next;var a=c.da;c.da=null;a()}};
return function(a){d.next={da:a};d=d.next;b.port2.postMessage(0)}}return"undefined"!==typeof document&&"onreadystatechange"in document.createElement("SCRIPT")?function(a){var b=document.createElement("SCRIPT");
b.onreadystatechange=function(){b.onreadystatechange=null;b.parentNode.removeChild(b);b=null;a();a=null};
document.documentElement.appendChild(b)}:function(a){m.setTimeout(a,0)}}
;function kb(){this.f=this.b=null}
var mb=new Va(function(){return new lb},function(a){a.reset()});
kb.prototype.add=function(a,b){var c=mb.get();c.set(a,b);this.f?this.f.next=c:this.b=c;this.f=c};
kb.prototype.remove=function(){var a=null;this.b&&(a=this.b,this.b=this.b.next,this.b||(this.f=null),a.next=null);return a};
function lb(){this.next=this.scope=this.b=null}
lb.prototype.set=function(a,b){this.b=a;this.scope=b;this.next=null};
lb.prototype.reset=function(){this.next=this.scope=this.b=null};function nb(a,b){ob||pb();qb||(ob(),qb=!0);rb.add(a,b)}
var ob;function pb(){if(-1!=String(m.Promise).indexOf("[native code]")){var a=m.Promise.resolve(void 0);ob=function(){a.then(sb)}}else ob=function(){var a=sb;
!sa(m.setImmediate)||m.Window&&m.Window.prototype&&!z("Edge")&&m.Window.prototype.setImmediate==m.setImmediate?(ib||(ib=jb()),ib(a)):m.setImmediate(a)}}
var qb=!1,rb=new kb;function sb(){for(var a;a=rb.remove();){try{a.b.call(a.scope)}catch(b){hb(b)}Wa(mb,a)}qb=!1}
;function tb(a,b,c){ma(a)?(this.date=ub(a,b||0,c||1),vb(this,c||1)):ta(a)?(this.date=ub(a.getFullYear(),a.getMonth(),a.getDate()),vb(this,a.getDate())):(this.date=new Date(za()),a=this.date.getDate(),this.date.setHours(0),this.date.setMinutes(0),this.date.setSeconds(0),this.date.setMilliseconds(0),vb(this,a))}
function ub(a,b,c){b=new Date(a,b,c);0<=a&&100>a&&b.setFullYear(b.getFullYear()-1900);return b}
k=tb.prototype;k.getFullYear=function(){return this.date.getFullYear()};
k.getMonth=function(){return this.date.getMonth()};
k.getDate=function(){return this.date.getDate()};
k.getTime=function(){return this.date.getTime()};
k.set=function(a){this.date=new Date(a.getFullYear(),a.getMonth(),a.getDate())};
k.add=function(a){if(a.h||a.f){var b=this.getMonth()+a.f+12*a.h,c=this.getFullYear()+Math.floor(b/12);b%=12;0>b&&(b+=12);a:{switch(b){case 1:var d=0!=c%4||0==c%100&&0!=c%400?28:29;break a;case 5:case 8:case 10:case 3:d=30;break a}d=31}d=Math.min(d,this.getDate());this.date.setDate(1);this.date.setFullYear(c);this.date.setMonth(b);this.date.setDate(d)}a.days&&(a=new Date((new Date(this.getFullYear(),this.getMonth(),this.getDate(),12)).getTime()+864E5*a.days),this.date.setDate(1),this.date.setFullYear(a.getFullYear()),
this.date.setMonth(a.getMonth()),this.date.setDate(a.getDate()),vb(this,a.getDate()))};
k.N=function(a){return[this.getFullYear(),Qa(this.getMonth()+1),Qa(this.getDate())].join(a?"-":"")+""};
k.equals=function(a){return!(!a||this.getFullYear()!=a.getFullYear()||this.getMonth()!=a.getMonth()||this.getDate()!=a.getDate())};
k.toString=function(){return this.N()};
function vb(a,b){a.getDate()!=b&&a.date.setUTCHours(a.date.getUTCHours()+(a.getDate()<b?1:-1))}
k.valueOf=function(){return this.date.valueOf()};
function wb(a,b,c,d,e,f,g){this.date=ma(a)?new Date(a,b||0,c||1,d||0,e||0,f||0,g||0):new Date(a&&a.getTime?a.getTime():za())}
u(wb,tb);wb.prototype.add=function(a){tb.prototype.add.call(this,a);a.b&&this.date.setUTCHours(this.date.getUTCHours()+a.b);a.minutes&&this.date.setUTCMinutes(this.date.getUTCMinutes()+a.minutes);a.seconds&&this.date.setUTCSeconds(this.date.getUTCSeconds()+a.seconds)};
wb.prototype.N=function(a){var b=tb.prototype.N.call(this,a);return a?b+" "+Qa(this.date.getHours())+":"+Qa(this.date.getMinutes())+":"+Qa(this.date.getSeconds()):b+"T"+Qa(this.date.getHours())+Qa(this.date.getMinutes())+Qa(this.date.getSeconds())};
wb.prototype.equals=function(a){return this.getTime()==a.getTime()};
wb.prototype.toString=function(){return this.N()};function xb(){this.h=this.h;this.i=this.i}
xb.prototype.h=!1;xb.prototype.L=function(){return this.h};
xb.prototype.dispose=function(){this.h||(this.h=!0,this.R())};
xb.prototype.R=function(){if(this.i)for(;this.i.length;)this.i.shift()()};
function yb(a){a&&"function"==typeof a.dispose&&a.dispose()}
;function zb(){return z("iPhone")&&!z("iPod")&&!z("iPad")}
function Ab(){return zb()||z("iPad")||z("iPod")}
;function Bb(a){Bb[" "](a);return a}
Bb[" "]=oa;function Cb(a,b){var c=Db;return Object.prototype.hasOwnProperty.call(c,a)?c[a]:c[a]=b(a)}
;var Eb=z("Opera"),A=z("Trident")||z("MSIE"),Fb=z("Edge"),Gb=Fb||A,Hb=z("Gecko")&&!(-1!=y.toLowerCase().indexOf("webkit")&&!z("Edge"))&&!(z("Trident")||z("MSIE"))&&!z("Edge"),Ib=-1!=y.toLowerCase().indexOf("webkit")&&!z("Edge"),Jb=z("Macintosh"),Kb=z("Windows"),Lb=z("Android"),Mb=zb(),Nb=z("iPad"),Ob=z("iPod"),Pb=Ab();function Qb(){var a=m.document;return a?a.documentMode:void 0}
var Rb;a:{var Sb="",Tb=function(){var a=y;if(Hb)return/rv:([^\);]+)(\)|;)/.exec(a);if(Fb)return/Edge\/([\d\.]+)/.exec(a);if(A)return/\b(?:MSIE|rv)[: ]([^\);]+)(\)|;)/.exec(a);if(Ib)return/WebKit\/(\S+)/.exec(a);if(Eb)return/(?:Version)[ \/]?(\S+)/.exec(a)}();
Tb&&(Sb=Tb?Tb[1]:"");if(A){var Ub=Qb();if(null!=Ub&&Ub>parseFloat(Sb)){Rb=String(Ub);break a}}Rb=Sb}var Vb=Rb,Db={};function Wb(a){return Cb(a,function(){return 0<=Ra(Vb,a)})}
var Xb;var Yb=m.document;Xb=Yb&&A?Qb()||("CSS1Compat"==Yb.compatMode?parseInt(Vb,10):5):void 0;var Zb=!Hb&&!A||A&&9<=Number(Xb)||Hb&&Wb("1.9.1"),$b=A&&!Wb("9");function ac(a){if(a.classList)return a.classList;a=a.className;return p(a)&&a.match(/\S+/g)||[]}
function B(a,b){return a.classList?a.classList.contains(b):Ha(ac(a),b)}
function C(a,b){a.classList?a.classList.add(b):B(a,b)||(a.className+=0<a.className.length?" "+b:b)}
function bc(a,b){if(a.classList)x(b,function(b){C(a,b)});
else{var c={};x(ac(a),function(a){c[a]=!0});
x(b,function(a){c[a]=!0});
a.className="";for(var d in c)a.className+=0<a.className.length?" "+d:d}}
function D(a,b){a.classList?a.classList.remove(b):B(a,b)&&(a.className=Da(ac(a),function(a){return a!=b}).join(" "))}
function cc(a,b){a.classList?x(b,function(b){D(a,b)}):a.className=Da(ac(a),function(a){return!Ha(b,a)}).join(" ")}
function F(a,b,c){c?C(a,b):D(a,b)}
function dc(a,b,c){B(a,b)&&(D(a,b),C(a,c))}
function ec(a,b){var c=!B(a,b);F(a,b,c)}
;var fc=z("Firefox"),gc=zb()||z("iPod"),hc=z("iPad"),ic=gb(),jc=fb(),kc=eb()&&!Ab();var lc=!A&&!eb();function mc(a,b){if(/-[a-z]/.test(b))return null;if(lc&&a.dataset){if(gb()&&!(b in a.dataset))return null;var c=a.dataset[b];return void 0===c?null:c}return a.getAttribute("data-"+String(b).replace(/([A-Z])/g,"-$1").toLowerCase())}
;function nc(){this.b="";this.f=oc}
nc.prototype.T=!0;nc.prototype.S=function(){return this.b};
var pc=/^(?:(?:https?|mailto|ftp):|[^:/?#]*(?:[/?#]|$))/i,oc={};function qc(a){var b=new nc;b.b=a;return b}
qc("about:blank");function rc(){this.b="";this.f=sc}
rc.prototype.T=!0;rc.prototype.S=function(){return this.b};
function tc(a){if(a instanceof rc&&a.constructor===rc&&a.f===sc)return a.b;qa(a);return"type_error:SafeHtml"}
var sc={};function uc(a){var b=new rc;b.b=a;return b}
uc("<!DOCTYPE html>");uc("");uc("<br>");function G(a,b){this.x=la(a)?a:0;this.y=la(b)?b:0}
G.prototype.equals=function(a){return a instanceof G&&(this==a?!0:this&&a?this.x==a.x&&this.y==a.y:!1)};
function vc(a,b){return new G(a.x-b.x,a.y-b.y)}
G.prototype.ceil=function(){this.x=Math.ceil(this.x);this.y=Math.ceil(this.y);return this};
G.prototype.floor=function(){this.x=Math.floor(this.x);this.y=Math.floor(this.y);return this};
G.prototype.round=function(){this.x=Math.round(this.x);this.y=Math.round(this.y);return this};function wc(a,b){this.width=a;this.height=b}
k=wc.prototype;k.Ua=function(){return this.width*this.height};
k.aspectRatio=function(){return this.width/this.height};
k.isEmpty=function(){return!this.Ua()};
k.ceil=function(){this.width=Math.ceil(this.width);this.height=Math.ceil(this.height);return this};
k.floor=function(){this.width=Math.floor(this.width);this.height=Math.floor(this.height);return this};
k.round=function(){this.width=Math.round(this.width);this.height=Math.round(this.height);return this};function xc(a){return a?new yc(zc(a)):Ba||(Ba=new yc)}
function H(a){return p(a)?document.getElementById(a):a}
function Ac(a,b){var c=b||document;return c.querySelectorAll&&c.querySelector?c.querySelectorAll("."+a):Bc(document,"*",a,b)}
function I(a,b){var c=b||document;if(c.getElementsByClassName)c=c.getElementsByClassName(a)[0];else{c=document;var d=b||c;c=d.querySelectorAll&&d.querySelector&&a?d.querySelector(a?"."+a:""):Bc(c,"*",a,b)[0]||null}return c||null}
function Bc(a,b,c,d){a=d||a;b=b&&"*"!=b?String(b).toUpperCase():"";if(a.querySelectorAll&&a.querySelector&&(b||c))return a.querySelectorAll(b+(c?"."+c:""));if(c&&a.getElementsByClassName){a=a.getElementsByClassName(c);if(b){d={};for(var e=0,f=0,g;g=a[f];f++)b==g.nodeName&&(d[e++]=g);d.length=e;return d}return a}a=a.getElementsByTagName(b||"*");if(c){d={};for(f=e=0;g=a[f];f++)b=g.className,"function"==typeof b.split&&Ha(b.split(/\s+/),c)&&(d[e++]=g);d.length=e;return d}return a}
function Cc(a,b){Za(b,function(b,d){b&&b.T&&(b=b.S());"style"==d?a.style.cssText=b:"class"==d?a.className=b:"for"==d?a.htmlFor=b:Dc.hasOwnProperty(d)?a.setAttribute(Dc[d],b):0==d.lastIndexOf("aria-",0)||0==d.lastIndexOf("data-",0)?a.setAttribute(d,b):a[d]=b})}
var Dc={cellpadding:"cellPadding",cellspacing:"cellSpacing",colspan:"colSpan",frameborder:"frameBorder",height:"height",maxlength:"maxLength",nonce:"nonce",role:"role",rowspan:"rowSpan",type:"type",usemap:"useMap",valign:"vAlign",width:"width"};function Ec(a){a=a.document;a=Fc(a)?a.documentElement:a.body;return new wc(a.clientWidth,a.clientHeight)}
function Gc(a){var b=Hc(a);a=Ic(a);return A&&Wb("10")&&a.pageYOffset!=b.scrollTop?new G(b.scrollLeft,b.scrollTop):new G(a.pageXOffset||b.scrollLeft,a.pageYOffset||b.scrollTop)}
function Hc(a){return a.scrollingElement?a.scrollingElement:!Ib&&Fc(a)?a.documentElement:a.body||a.documentElement}
function Ic(a){return a.parentWindow||a.defaultView}
function Fc(a){return"CSS1Compat"==a.compatMode}
function Jc(a){a&&a.parentNode&&a.parentNode.removeChild(a)}
function Kc(a){return Zb&&void 0!=a.children?a.children:Da(a.childNodes,function(a){return 1==a.nodeType})}
function Lc(a){return ta(a)&&1==a.nodeType}
function Mc(a,b){if(!a||!b)return!1;if(a.contains&&1==b.nodeType)return a==b||a.contains(b);if("undefined"!=typeof a.compareDocumentPosition)return a==b||!!(a.compareDocumentPosition(b)&16);for(;b&&a!=b;)b=b.parentNode;return b==a}
function zc(a){return 9==a.nodeType?a:a.ownerDocument||a.document}
function Nc(a,b){if("textContent"in a)a.textContent=b;else if(3==a.nodeType)a.data=String(b);else if(a.firstChild&&3==a.firstChild.nodeType){for(;a.lastChild!=a.firstChild;)a.removeChild(a.lastChild);a.firstChild.data=String(b)}else{for(var c;c=a.firstChild;)a.removeChild(c);a.appendChild(zc(a).createTextNode(String(b)))}}
function Oc(a,b){var c=[];return Pc(a,b,c,!0)?c[0]:void 0}
function Pc(a,b,c,d){if(null!=a)for(a=a.firstChild;a;){if(b(a)&&(c.push(a),d)||Pc(a,b,c,d))return!0;a=a.nextSibling}return!1}
var Qc={SCRIPT:1,STYLE:1,HEAD:1,IFRAME:1,OBJECT:1},Rc={IMG:" ",BR:"\n"};function Sc(a){var b;if((b="A"==a.tagName||"INPUT"==a.tagName||"TEXTAREA"==a.tagName||"SELECT"==a.tagName||"BUTTON"==a.tagName?!a.disabled&&(!Tc(a)||Uc(a)):Tc(a)&&Uc(a))&&A){var c;!sa(a.getBoundingClientRect)||A&&null==a.parentElement?c={height:a.offsetHeight,width:a.offsetWidth}:c=a.getBoundingClientRect();a=null!=c&&0<c.height&&0<c.width}else a=b;return a}
function Tc(a){return A&&!Wb("9")?(a=a.getAttributeNode("tabindex"),null!=a&&a.specified):a.hasAttribute("tabindex")}
function Uc(a){a=a.tabIndex;return ma(a)&&0<=a&&32768>a}
function Vc(a){if($b&&null!==a&&"innerText"in a)a=a.innerText.replace(/(\r\n|\r|\n)/g,"\n");else{var b=[];Wc(a,b,!0);a=b.join("")}a=a.replace(/ \xAD /g," ").replace(/\xAD/g,"");a=a.replace(/\u200B/g,"");$b||(a=a.replace(/ +/g," "));" "!=a&&(a=a.replace(/^\s*/,""));return a}
function Wc(a,b,c){if(!(a.nodeName in Qc))if(3==a.nodeType)c?b.push(String(a.nodeValue).replace(/(\r\n|\r|\n)/g,"")):b.push(a.nodeValue);else if(a.nodeName in Rc)b.push(Rc[a.nodeName]);else for(a=a.firstChild;a;)Wc(a,b,c),a=a.nextSibling}
function Xc(a,b,c,d){if(!b&&!c)return null;var e=b?String(b).toUpperCase():null;return Yc(a,function(a){return(!e||a.nodeName==e)&&(!c||p(a.className)&&Ha(a.className.split(/\s+/),c))},!0,d)}
function J(a,b){return Xc(a,null,b,void 0)}
function Yc(a,b,c,d){a&&!c&&(a=a.parentNode);for(c=0;a&&(null==d||c<=d);){if(b(a))return a;a=a.parentNode;c++}return null}
function yc(a){this.b=a||m.document||document}
yc.prototype.getElementsByTagName=function(a,b){return(b||this.b).getElementsByTagName(String(a))};
yc.prototype.createElement=function(a){return this.b.createElement(String(a))};
yc.prototype.appendChild=function(a,b){a.appendChild(b)};
yc.prototype.isElement=Lc;var Zc="StopIteration"in m?m.StopIteration:{message:"StopIteration",stack:""};function $c(){}
$c.prototype.next=function(){throw Zc;};
$c.prototype.J=function(){return this};
function ad(a){if(a instanceof $c)return a;if("function"==typeof a.J)return a.J(!1);if(ra(a)){var b=0,c=new $c;c.next=function(){for(;;){if(b>=a.length)throw Zc;if(b in a)return a[b++];b++}};
return c}throw Error("Not implemented");}
function bd(a,b){if(ra(a))try{x(a,b,void 0)}catch(c){if(c!==Zc)throw c;}else{a=ad(a);try{for(;;)b.call(void 0,a.next(),void 0,a)}catch(c){if(c!==Zc)throw c;}}}
function cd(a){if(ra(a))return Ja(a);a=ad(a);var b=[];bd(a,function(a){b.push(a)});
return b}
;function dd(a,b,c,d){this.top=a;this.right=b;this.bottom=c;this.left=d}
dd.prototype.getHeight=function(){return this.bottom-this.top};
dd.prototype.ceil=function(){this.top=Math.ceil(this.top);this.right=Math.ceil(this.right);this.bottom=Math.ceil(this.bottom);this.left=Math.ceil(this.left);return this};
dd.prototype.floor=function(){this.top=Math.floor(this.top);this.right=Math.floor(this.right);this.bottom=Math.floor(this.bottom);this.left=Math.floor(this.left);return this};
dd.prototype.round=function(){this.top=Math.round(this.top);this.right=Math.round(this.right);this.bottom=Math.round(this.bottom);this.left=Math.round(this.left);return this};function ed(a,b,c,d){this.left=a;this.top=b;this.width=c;this.height=d}
ed.prototype.ceil=function(){this.left=Math.ceil(this.left);this.top=Math.ceil(this.top);this.width=Math.ceil(this.width);this.height=Math.ceil(this.height);return this};
ed.prototype.floor=function(){this.left=Math.floor(this.left);this.top=Math.floor(this.top);this.width=Math.floor(this.width);this.height=Math.floor(this.height);return this};
ed.prototype.round=function(){this.left=Math.round(this.left);this.top=Math.round(this.top);this.width=Math.round(this.width);this.height=Math.round(this.height);return this};function fd(a,b,c){if(p(b))(b=gd(a,b))&&(a.style[b]=c);else for(var d in b){c=a;var e=b[d],f=gd(c,d);f&&(c.style[f]=e)}}
var hd={};function gd(a,b){var c=hd[b];if(!c){var d=Ta(b);c=d;void 0===a.style[d]&&(d=(Ib?"Webkit":Hb?"Moz":A?"ms":Eb?"O":null)+Ua(d),void 0!==a.style[d]&&(c=d));hd[b]=c}return c}
function id(a,b){var c=zc(a);return c.defaultView&&c.defaultView.getComputedStyle&&(c=c.defaultView.getComputedStyle(a,null))?c[b]||c.getPropertyValue(b)||"":""}
function jd(a,b){return id(a,b)||(a.currentStyle?a.currentStyle[b]:null)||a.style&&a.style[b]}
function kd(a){try{var b=a.getBoundingClientRect()}catch(c){return{left:0,top:0,right:0,bottom:0}}A&&a.ownerDocument.body&&(a=a.ownerDocument,b.left-=a.documentElement.clientLeft+a.body.clientLeft,b.top-=a.documentElement.clientTop+a.body.clientTop);return b}
function ld(a){if(A&&!(8<=Number(Xb)))return a.offsetParent;var b=zc(a),c=jd(a,"position"),d="fixed"==c||"absolute"==c;for(a=a.parentNode;a&&a!=b;a=a.parentNode)if(11==a.nodeType&&a.host&&(a=a.host),c=jd(a,"position"),d=d&&"static"==c&&a!=b.documentElement&&a!=b.body,!d&&(a.scrollWidth>a.clientWidth||a.scrollHeight>a.clientHeight||"fixed"==c||"absolute"==c||"relative"==c))return a;return null}
function md(a){for(var b=new dd(0,Infinity,Infinity,0),c=xc(a),d=c.b.body,e=c.b.documentElement,f=Hc(c.b);a=ld(a);)if(!(A&&0==a.clientWidth||Ib&&0==a.clientHeight&&a==d)&&a!=d&&a!=e&&"visible"!=jd(a,"overflow")){var g=nd(a),h=new G(a.clientLeft,a.clientTop);g.x+=h.x;g.y+=h.y;b.top=Math.max(b.top,g.y);b.right=Math.min(b.right,g.x+a.clientWidth);b.bottom=Math.min(b.bottom,g.y+a.clientHeight);b.left=Math.max(b.left,g.x)}d=f.scrollLeft;f=f.scrollTop;b.left=Math.max(b.left,d);b.top=Math.max(b.top,f);c=
Ec(Ic(c.b)||window);b.right=Math.min(b.right,d+c.width);b.bottom=Math.min(b.bottom,f+c.height);return 0<=b.top&&0<=b.left&&b.bottom>b.top&&b.right>b.left?b:null}
function nd(a){var b=zc(a),c=new G(0,0);var d=b?zc(b):document;d=!A||9<=Number(Xb)||Fc(xc(d).b)?d.documentElement:d.body;if(a==d)return c;a=kd(a);b=Gc(xc(b).b);c.x=a.left+b.x;c.y=a.top+b.y;return c}
function od(a){a=kd(a);return new G(a.left,a.top)}
function pd(a,b){"number"==typeof a&&(a=(b?Math.round(a):a)+"px");return a}
function qd(a){var b=rd;if("none"!=jd(a,"display"))return b(a);var c=a.style,d=c.display,e=c.visibility,f=c.position;c.visibility="hidden";c.position="absolute";c.display="inline";a=b(a);c.display=d;c.position=f;c.visibility=e;return a}
function rd(a){var b=a.offsetWidth,c=a.offsetHeight,d=Ib&&!b&&!c;return la(b)&&!d||!a.getBoundingClientRect?new wc(b,c):(a=kd(a),new wc(a.right-a.left,a.bottom-a.top))}
function sd(a){var b=nd(a);a=qd(a);return new ed(b.x,b.y,a.width,a.height)}
function td(a){return"rtl"==jd(a,"direction")}
function ud(a,b){if(/^\d+px?$/.test(b))return parseInt(b,10);var c=a.style.left,d=a.runtimeStyle.left;a.runtimeStyle.left=a.currentStyle.left;a.style.left=b;var e=a.style.pixelLeft;a.style.left=c;a.runtimeStyle.left=d;return+e}
function vd(a,b){var c=a.currentStyle?a.currentStyle[b]:null;return c?ud(a,c):0}
var wd={thin:2,medium:4,thick:6};function xd(a,b){if("none"==(a.currentStyle?a.currentStyle[b+"Style"]:null))return 0;var c=a.currentStyle?a.currentStyle[b+"Width"]:null;return c in wd?wd[c]:ud(a,c)}
;var yd=function(){if(Kb){var a=/Windows NT ([0-9.]+)/;return(a=a.exec(y))?a[1]:"0"}return Jb?(a=/10[_.][0-9_.]+/,(a=a.exec(y))?a[0].replace(/_/g,"."):"10"):Lb?(a=/Android\s+([^\);]+)(\)|;)/,(a=a.exec(y))?a[1]:""):Mb||Nb||Ob?(a=/(?:iPhone|CPU)\s+OS\s+(\S+)/,(a=a.exec(y))?a[1].replace(/_/g,"."):""):""}();function zd(a){return(a=a.exec(y))?a[1]:""}
var Ad=function(){if(fc)return zd(/Firefox\/([0-9.]+)/);if(A||Fb||Eb)return Vb;if(jc)return Ab()?zd(/CriOS\/([0-9.]+)/):zd(/Chrome\/([0-9.]+)/);if(kc&&!Ab())return zd(/Version\/([0-9.]+)/);if(gc||hc){var a=/Version\/(\S+).*Mobile\/(\S+)/.exec(y);if(a)return a[1]+"."+a[2]}else if(ic)return(a=zd(/Android\s+([0-9.]+)/))?a:zd(/Version\/([0-9.]+)/);return""}();function Bd(a,b,c,d,e,f,g){var h;if(h=c.offsetParent){var l="HTML"==h.tagName||"BODY"==h.tagName;if(!l||"static"!=jd(h,"position")){var n=nd(h);if(!l){l=td(h);var w;if(w=l){if(w=kc)w=0<=Ra(Ad,10);var E;if(E=Pb)E=0<=Ra(yd,10);w=Hb||w||E}l=w?-h.scrollLeft:!l||Gb&&Wb("8")||"visible"==jd(h,"overflowX")?h.scrollLeft:h.scrollWidth-h.clientWidth-h.scrollLeft;n=vc(n,new G(l,h.scrollTop))}}}h=n||new G;n=sd(a);if(l=md(a)){var v=new ed(l.left,l.top,l.right-l.left,l.bottom-l.top);l=Math.max(n.left,v.left);w=
Math.min(n.left+n.width,v.left+v.width);l<=w&&(E=Math.max(n.top,v.top),v=Math.min(n.top+n.height,v.top+v.height),E<=v&&(n.left=l,n.top=E,n.width=w-l,n.height=v-E))}l=xc(a);E=xc(c);if(l.b!=E.b){w=l.b.body;E=Ic(E.b);v=new G(0,0);var P=(P=zc(w))?Ic(P):window;b:{try{Bb(P.parent);var N=!0;break b}catch(ri){}N=!1}if(N){N=w;do{var Je=P==E?nd(N):od(N);v.x+=Je.x;v.y+=Je.y}while(P&&P!=E&&P!=P.parent&&(N=P.frameElement)&&(P=P.parent))}N=vc(v,nd(w));!A||9<=Number(Xb)||Fc(l.b)||(N=vc(N,Gc(l.b)));n.left+=N.x;n.top+=
N.y}a=Cd(a,b);b=n.left;a&4?b+=n.width:a&2&&(b+=n.width/2);b=new G(b,n.top+(a&1?n.height:0));b=vc(b,h);e&&(b.x+=(a&4?-1:1)*e.x,b.y+=(a&1?-1:1)*e.y);var na;g&&(na=md(c))&&(na.top-=h.y,na.right-=h.x,na.bottom-=h.y,na.left-=h.x);return Dd(b,c,d,f,na,g,void 0)}
function Dd(a,b,c,d,e,f,g){a=new G(a.x,a.y);var h=Cd(b,c);c=qd(b);g=g?new wc(g.width,g.height):new wc(c.width,c.height);a=new G(a.x,a.y);g=new wc(g.width,g.height);var l=0;if(d||0!=h)h&4?a.x-=g.width+(d?d.right:0):h&2?a.x-=g.width/2:d&&(a.x+=d.left),h&1?a.y-=g.height+(d?d.bottom:0):d&&(a.y+=d.top);if(f){if(e){d=a;h=g;l=0;65==(f&65)&&(d.x<e.left||d.x>=e.right)&&(f&=-2);132==(f&132)&&(d.y<e.top||d.y>=e.bottom)&&(f&=-5);d.x<e.left&&f&1&&(d.x=e.left,l|=1);if(f&16){var n=d.x;d.x<e.left&&(d.x=e.left,l|=
4);d.x+h.width>e.right&&(h.width=Math.min(e.right-d.x,n+h.width-e.left),h.width=Math.max(h.width,0),l|=4)}d.x+h.width>e.right&&f&1&&(d.x=Math.max(e.right-h.width,e.left),l|=1);f&2&&(l|=(d.x<e.left?16:0)|(d.x+h.width>e.right?32:0));d.y<e.top&&f&4&&(d.y=e.top,l|=2);f&32&&(n=d.y,d.y<e.top&&(d.y=e.top,l|=8),d.y+h.height>e.bottom&&(h.height=Math.min(e.bottom-d.y,n+h.height-e.top),h.height=Math.max(h.height,0),l|=8));d.y+h.height>e.bottom&&f&4&&(d.y=Math.max(e.bottom-h.height,e.top),l|=2);f&8&&(l|=(d.y<
e.top?64:0)|(d.y+h.height>e.bottom?128:0));e=l}else e=256;l=e}f=new ed(0,0,0,0);f.left=a.x;f.top=a.y;f.width=g.width;f.height=g.height;e=l;if(e&496)return e;g=new G(f.left,f.top);g instanceof G?(a=g.x,g=g.y):(a=g,g=void 0);b.style.left=pd(a,!1);b.style.top=pd(g,!1);g=new wc(f.width,f.height);c==g||c&&g&&c.width==g.width&&c.height==g.height||(c=g,g=Fc(xc(zc(b)).b),!A||Wb("10")||g&&Wb("8")?(b=b.style,Hb?b.MozBoxSizing="border-box":Ib?b.WebkitBoxSizing="border-box":b.boxSizing="border-box",b.width=Math.max(c.width,
0)+"px",b.height=Math.max(c.height,0)+"px"):(a=b.style,g?(A?(g=vd(b,"paddingLeft"),f=vd(b,"paddingRight"),d=vd(b,"paddingTop"),h=vd(b,"paddingBottom"),g=new dd(d,f,h,g)):(g=id(b,"paddingLeft"),f=id(b,"paddingRight"),d=id(b,"paddingTop"),h=id(b,"paddingBottom"),g=new dd(parseFloat(d),parseFloat(f),parseFloat(h),parseFloat(g))),!A||9<=Number(Xb)?(f=id(b,"borderLeftWidth"),d=id(b,"borderRightWidth"),h=id(b,"borderTopWidth"),b=id(b,"borderBottomWidth"),b=new dd(parseFloat(h),parseFloat(d),parseFloat(b),
parseFloat(f))):(f=xd(b,"borderLeft"),d=xd(b,"borderRight"),h=xd(b,"borderTop"),b=xd(b,"borderBottom"),b=new dd(h,d,b,f)),a.pixelWidth=c.width-b.left-g.left-g.right-b.right,a.pixelHeight=c.height-b.top-g.top-g.bottom-b.bottom):(a.pixelWidth=c.width,a.pixelHeight=c.height)));return e}
function Cd(a,b){return(b&8&&td(a)?b^4:b)&-9}
;function Ed(a){this.b=0;this.o=void 0;this.i=this.f=this.h=null;this.j=this.l=!1;if(a!=oa)try{var b=this;a.call(void 0,function(a){Fd(b,2,a)},function(a){Fd(b,3,a)})}catch(c){Fd(this,3,c)}}
function Gd(){this.next=this.context=this.f=this.h=this.b=null;this.i=!1}
Gd.prototype.reset=function(){this.context=this.f=this.h=this.b=null;this.i=!1};
var Hd=new Va(function(){return new Gd},function(a){a.reset()});
function Id(a,b,c){var d=Hd.get();d.h=a;d.f=b;d.context=c;return d}
Ed.prototype.then=function(a,b,c){return Jd(this,sa(a)?a:null,sa(b)?b:null,c)};
Ed.prototype.then=Ed.prototype.then;Ed.prototype.$goog_Thenable=!0;Ed.prototype.cancel=function(a){0==this.b&&nb(function(){var b=new Kd(a);Ld(this,b)},this)};
function Ld(a,b){if(0==a.b)if(a.h){var c=a.h;if(c.f){for(var d=0,e=null,f=null,g=c.f;g&&(g.i||(d++,g.b==a&&(e=g),!(e&&1<d)));g=g.next)e||(f=g);e&&(0==c.b&&1==d?Ld(c,b):(f?(d=f,d.next==c.i&&(c.i=d),d.next=d.next.next):Md(c),Nd(c,e,3,b)))}a.h=null}else Fd(a,3,b)}
function Od(a,b){a.f||2!=a.b&&3!=a.b||Pd(a);a.i?a.i.next=b:a.f=b;a.i=b}
function Jd(a,b,c,d){var e=Id(null,null,null);e.b=new Ed(function(a,g){e.h=b?function(c){try{var e=b.call(d,c);a(e)}catch(n){g(n)}}:a;
e.f=c?function(b){try{var e=c.call(d,b);!la(e)&&b instanceof Kd?g(b):a(e)}catch(n){g(n)}}:g});
e.b.h=a;Od(a,e);return e.b}
Ed.prototype.B=function(a){this.b=0;Fd(this,2,a)};
Ed.prototype.P=function(a){this.b=0;Fd(this,3,a)};
function Fd(a,b,c){if(0==a.b){a===c&&(b=3,c=new TypeError("Promise cannot resolve to itself"));a.b=1;a:{var d=c,e=a.B,f=a.P;if(d instanceof Ed){Od(d,Id(e||oa,f||null,a));var g=!0}else{if(d)try{var h=!!d.$goog_Thenable}catch(n){h=!1}else h=!1;if(h)d.then(e,f,a),g=!0;else{if(ta(d))try{var l=d.then;if(sa(l)){Qd(d,l,e,f,a);g=!0;break a}}catch(n){f.call(a,n);g=!0;break a}g=!1}}}g||(a.o=c,a.b=b,a.h=null,Pd(a),3!=b||c instanceof Kd||Rd(a,c))}}
function Qd(a,b,c,d,e){function f(a){h||(h=!0,d.call(e,a))}
function g(a){h||(h=!0,c.call(e,a))}
var h=!1;try{b.call(a,g,f)}catch(l){f(l)}}
function Pd(a){a.l||(a.l=!0,nb(a.w,a))}
function Md(a){var b=null;a.f&&(b=a.f,a.f=b.next,b.next=null);a.f||(a.i=null);return b}
Ed.prototype.w=function(){for(var a;a=Md(this);)Nd(this,a,this.b,this.o);this.l=!1};
function Nd(a,b,c,d){if(3==c&&b.f&&!b.i)for(;a&&a.j;a=a.h)a.j=!1;if(b.b)b.b.h=null,Sd(b,c,d);else try{b.i?b.h.call(b.context):Sd(b,c,d)}catch(e){Td.call(null,e)}Wa(Hd,b)}
function Sd(a,b,c){2==b?a.h.call(a.context,c):a.f&&a.f.call(a.context,c)}
function Rd(a,b){a.j=!0;nb(function(){a.j&&Td.call(null,b)})}
var Td=hb;function Kd(a){Aa.call(this,a)}
u(Kd,Aa);Kd.prototype.name="cancel";function K(a){xb.call(this);this.o=1;this.j=[];this.l=0;this.b=[];this.f={};this.w=!!a}
u(K,xb);k=K.prototype;k.subscribe=function(a,b,c){var d=this.f[a];d||(d=this.f[a]=[]);var e=this.o;this.b[e]=a;this.b[e+1]=b;this.b[e+2]=c;this.o=e+3;d.push(e);return e};
function Ud(a,b){var c=!1,d=a.subscribe("ROOT_MENU_REMOVED",function(a){c||(c=!0,this.I(d),b.apply(void 0,arguments))},a)}
function Vd(a,b,c){if(b=a.f[b]){var d=a.b;(b=Ga(b,function(a){return d[a+1]==c&&void 0==d[a+2]}))&&a.I(b)}}
k.I=function(a){var b=this.b[a];if(b){var c=this.f[b];if(0!=this.l)this.j.push(a),this.b[a+1]=oa;else{if(c){var d=Ca(c,a);0<=d&&Array.prototype.splice.call(c,d,1)}delete this.b[a];delete this.b[a+1];delete this.b[a+2]}}return!!b};
k.A=function(a,b){var c=this.f[a];if(c){for(var d=Array(arguments.length-1),e=1,f=arguments.length;e<f;e++)d[e-1]=arguments[e];if(this.w)for(e=0;e<c.length;e++){var g=c[e];Wd(this.b[g+1],this.b[g+2],d)}else{this.l++;try{for(e=0,f=c.length;e<f;e++)g=c[e],this.b[g+1].apply(this.b[g+2],d)}finally{if(this.l--,0<this.j.length&&0==this.l)for(;c=this.j.pop();)this.I(c)}}return 0!=e}return!1};
function Wd(a,b,c){nb(function(){a.apply(b,c)})}
k.clear=function(a){if(a){var b=this.f[a];b&&(x(b,this.I,this),delete this.f[a])}else this.b.length=0,this.f={}};
function Xd(a,b){if(b){var c=a.f[b];return c?c.length:0}c=0;for(var d in a.f)c+=Xd(a,d);return c}
k.R=function(){K.u.R.call(this);this.clear();this.j.length=0};function Yd(){}
;function Zd(){}
u(Zd,Yd);Zd.prototype.clear=function(){var a=cd(this.J(!0)),b=this;x(a,function(a){b.remove(a)})};function $d(a){this.b=a}
u($d,Zd);k=$d.prototype;k.isAvailable=function(){if(!this.b)return!1;try{return this.b.setItem("__sak","1"),this.b.removeItem("__sak"),!0}catch(a){return!1}};
k.set=function(a,b){try{this.b.setItem(a,b)}catch(c){if(0==this.b.length)throw"Storage mechanism: Storage disabled";throw"Storage mechanism: Quota exceeded";}};
k.get=function(a){a=this.b.getItem(a);if(!p(a)&&null!==a)throw"Storage mechanism: Invalid value was encountered";return a};
k.remove=function(a){this.b.removeItem(a)};
k.J=function(a){var b=0,c=this.b,d=new $c;d.next=function(){if(b>=c.length)throw Zc;var d=c.key(b++);if(a)return d;d=c.getItem(d);if(!p(d))throw"Storage mechanism: Invalid value was encountered";return d};
return d};
k.clear=function(){this.b.clear()};
k.key=function(a){return this.b.key(a)};function ae(){var a=null;try{a=window.localStorage||null}catch(b){}this.b=a}
u(ae,$d);function be(){var a=null;try{a=window.sessionStorage||null}catch(b){}this.b=a}
u(be,$d);var ce=/^(?:([^:/?#.]+):)?(?:\/\/(?:([^/?#]*)@)?([^/#?]*?)(?::([0-9]+))?(?=[/#?]|$))?([^?#]+)?(?:\?([^#]*))?(?:#([\s\S]*))?$/;function de(a){return a?decodeURI(a):a}
function ee(a,b){if(!b)return a;var c=a.indexOf("#");0>c&&(c=a.length);var d=a.indexOf("?");if(0>d||d>c){d=c;var e=""}else e=a.substring(d+1,c);c=[a.substr(0,d),e,a.substr(c)];d=c[1];c[1]=b?d?d+"&"+b:b:d;return c[0]+(c[1]?"?"+c[1]:"")+c[2]}
function fe(a,b,c){if("array"==qa(b))for(var d=0;d<b.length;d++)fe(a,String(b[d]),c);else null!=b&&c.push(a+(""===b?"":"="+encodeURIComponent(String(b))))}
function ge(a,b){for(var c=[],d=b||0;d<a.length;d+=2)fe(a[d],a[d+1],c);return c.join("&")}
function he(a){var b=[],c;for(c in a)fe(c,a[c],b);return b.join("&")}
function ie(a,b){var c=2==arguments.length?ge(arguments[1],0):ge(arguments,1);return ee(a,c)}
function je(a,b){var c=he(b);return ee(a,c)}
;var ke=window.yt&&window.yt.config_||window.ytcfg&&window.ytcfg.data_||{};t("yt.config_",ke);function le(a){var b=arguments;if(1<b.length)ke[b[0]]=b[1];else{b=b[0];for(var c in b)ke[c]=b[c]}}
function L(a,b){return a in ke?ke[a]:b}
;function me(a){return a&&window.yterr?function(){try{return a.apply(this,arguments)}catch(b){ne(b)}}:a}
function ne(a){var b=q("yt.logging.errors.log");b?b(a,void 0,void 0,void 0,void 0):(b=L("ERRORS",[]),b.push([a,void 0,void 0,void 0,void 0]),le("ERRORS",b))}
;function M(a,b){sa(a)&&(a=me(a));return window.setTimeout(a,b)}
;var oe=q("ytPubsubPubsubInstance")||new K;K.prototype.subscribe=K.prototype.subscribe;K.prototype.unsubscribeByKey=K.prototype.I;K.prototype.publish=K.prototype.A;K.prototype.clear=K.prototype.clear;t("ytPubsubPubsubInstance",oe);var pe=q("ytPubsubPubsubSubscribedKeys")||{};t("ytPubsubPubsubSubscribedKeys",pe);var qe=q("ytPubsubPubsubTopicToKeys")||{};t("ytPubsubPubsubTopicToKeys",qe);var re=q("ytPubsubPubsubIsSynchronous")||{};t("ytPubsubPubsubIsSynchronous",re);
function se(a,b,c){var d=te();if(d){var e=d.subscribe(a,function(){var d=arguments;var g=function(){pe[e]&&b.apply(c||window,d)};
try{re[a]?g():M(g,0)}catch(h){ne(h)}},c);
pe[e]=!0;qe[a]||(qe[a]=[]);qe[a].push(e);return e}return 0}
function ue(a){var b=te();b&&(ma(a)?a=[a]:p(a)&&(a=[parseInt(a,10)]),x(a,function(a){b.unsubscribeByKey(a);delete pe[a]}))}
function O(a,b){var c=te();return c?c.publish.apply(c,arguments):!1}
function ve(a,b){re[a]=!0;var c=te();c&&c.publish.apply(c,arguments);re[a]=!1}
function te(){return q("ytPubsubPubsubInstance")}
;function we(a,b,c){a&&(a.dataset?a.dataset[xe(b)]=String(c):a.setAttribute("data-"+b,c))}
function ye(a,b){return a?a.dataset?a.dataset[xe(b)]:a.getAttribute("data-"+b):null}
function ze(a,b){a&&(a.dataset?delete a.dataset[xe(b)]:a.removeAttribute("data-"+b))}
var Ae={};function xe(a){return Ae[a]||(Ae[a]=String(a).replace(/\-([a-z])/g,function(a,c){return c.toUpperCase()}))}
;function Q(a,b){this.version=a;this.args=b}
;function R(a,b){this.topic=a;this.b=b}
R.prototype.toString=function(){return this.topic};function Be(a){var b=void 0;isNaN(b)&&(b=void 0);var c=q("yt.scheduler.instance.addJob");c?c(a,1,b):void 0===b?a():M(a,b||0)}
;function Ce(){}
function De(){}
Ce.prototype=aa(De.prototype);Ce.prototype.constructor=Ce;if(fa)fa(Ce,De);else for(var Ee in De)if("prototype"!=Ee)if(Object.defineProperties){var Fe=Object.getOwnPropertyDescriptor(De,Ee);Fe&&Object.defineProperty(Ce,Ee,Fe)}else Ce[Ee]=De[Ee];Ce.u=De.prototype;Ce.prototype.start=function(){var a=q("yt.scheduler.instance.start");a&&a()};
pa(Ce);Ce.m();var Ge=q("ytPubsub2Pubsub2Instance")||new K;K.prototype.subscribe=K.prototype.subscribe;K.prototype.unsubscribeByKey=K.prototype.I;K.prototype.publish=K.prototype.A;K.prototype.clear=K.prototype.clear;t("ytPubsub2Pubsub2Instance",Ge);var He=q("ytPubsub2Pubsub2SubscribedKeys")||{};t("ytPubsub2Pubsub2SubscribedKeys",He);var Ie=q("ytPubsub2Pubsub2TopicToKeys")||{};t("ytPubsub2Pubsub2TopicToKeys",Ie);var Ke=q("ytPubsub2Pubsub2IsAsync")||{};t("ytPubsub2Pubsub2IsAsync",Ke);
t("ytPubsub2Pubsub2SkipSubKey",null);function S(a,b){var c=Le();c&&c.publish.call(c,a.toString(),a,b)}
function Me(a,b,c){var d=Le();if(!d)return 0;var e=d.subscribe(a.toString(),function(d,g){var f=q("ytPubsub2Pubsub2SkipSubKey");f&&f==e||(f=function(){if(He[e])try{if(g&&a instanceof R&&a!=d)try{var f=a.b,h=g;if(!h.args||!h.version)throw Error("yt.pubsub2.Data.deserialize(): serializedData is incomplete.");try{if(!f.Aa){var w=new f;f.Aa=w.version}var E=f.Aa}catch(v){}if(!E||h.version!=E)throw Error("yt.pubsub2.Data.deserialize(): serializedData version is incompatible.");try{g=Reflect.construct(f,
Ja(h.args))}catch(v){throw v.message="yt.pubsub2.Data.deserialize(): "+v.message,v;}}catch(v){throw v.message="yt.pubsub2.pubsub2 cross-binary conversion error for "+a.toString()+": "+v.message,v;}b.call(c||window,g)}catch(v){ne(v)}},Ke[a.toString()]?q("yt.scheduler.instance")?Be(f):M(f,0):f())});
He[e]=!0;Ie[a.toString()]||(Ie[a.toString()]=[]);Ie[a.toString()].push(e);return e}
function Ne(a){var b=Le();b&&(ma(a)&&(a=[a]),x(a,function(a){b.unsubscribeByKey(a);delete He[a]}))}
function Le(){return q("ytPubsub2Pubsub2Instance")}
;var Oe=0;function Pe(a){var b=a.__yt_uid_key;b||(b=Qe(),a.__yt_uid_key=b);return b}
function Re(a,b){a=H(a);b=H(b);return!!Yc(a,function(a){return a===b},!0,void 0)}
function Se(a,b){var c=Bc(document,a,null,b);return c.length?c[0]:null}
function Te(){var a=document,b;Fa(["fullscreenElement","webkitFullscreenElement","mozFullScreenElement","msFullscreenElement"],function(c){b=a[c];return!!b});
return b}
function Ue(){F(document.body,"hide-players",!1);x(Ac("preserve-players"),function(a){D(a,"preserve-players")})}
var Qe=q("ytDomDomGetNextId")||function(){return++Oe};
t("ytDomDomGetNextId",Qe);var Ve={stopImmediatePropagation:1,stopPropagation:1,preventMouseEvent:1,preventManipulation:1,preventDefault:1,layerX:1,layerY:1,screenX:1,screenY:1,scale:1,rotation:1,webkitMovementX:1,webkitMovementY:1};
function We(a){this.type="";this.state=this.source=this.data=this.currentTarget=this.relatedTarget=this.target=null;this.charCode=this.keyCode=0;this.metaKey=this.shiftKey=this.ctrlKey=this.altKey=!1;this.clientY=this.clientX=0;this.changedTouches=this.touches=null;if(a=a||window.event){this.event=a;for(var b in a)b in Ve||(this[b]=a[b]);(b=a.target||a.srcElement)&&3==b.nodeType&&(b=b.parentNode);this.target=b;if(b=a.relatedTarget)try{b=b.nodeName?b:null}catch(c){b=null}else"mouseover"==this.type?
b=a.fromElement:"mouseout"==this.type&&(b=a.toElement);this.relatedTarget=b;this.clientX=void 0!=a.clientX?a.clientX:a.pageX;this.clientY=void 0!=a.clientY?a.clientY:a.pageY;this.keyCode=a.keyCode?a.keyCode:a.which;this.charCode=a.charCode||("keypress"==this.type?this.keyCode:0);this.altKey=a.altKey;this.ctrlKey=a.ctrlKey;this.shiftKey=a.shiftKey;this.metaKey=a.metaKey}}
We.prototype.preventDefault=function(){this.event&&(this.event.returnValue=!1,this.event.preventDefault&&this.event.preventDefault())};
We.prototype.stopPropagation=function(){this.event&&(this.event.cancelBubble=!0,this.event.stopPropagation&&this.event.stopPropagation())};
We.prototype.stopImmediatePropagation=function(){this.event&&(this.event.cancelBubble=!0,this.event.stopImmediatePropagation&&this.event.stopImmediatePropagation())};var bb=q("ytEventsEventsListeners")||{};t("ytEventsEventsListeners",bb);var Xe=q("ytEventsEventsCounter")||{count:0};t("ytEventsEventsCounter",Xe);function Ye(a,b,c,d){d=void 0===d?!1:d;a.addEventListener&&("mouseenter"!=b||"onmouseenter"in document?"mouseleave"!=b||"onmouseenter"in document?"mousewheel"==b&&"MozBoxSizing"in document.documentElement.style&&(b="MozMousePixelScroll"):b="mouseout":b="mouseover");return ab(function(e){return!!e.length&&e[0]==a&&e[1]==b&&e[2]==c&&e[4]==!!d})}
function T(a,b,c,d){d=void 0===d?!1:d;if(!a||!a.addEventListener&&!a.attachEvent)return"";var e=Ye(a,b,c,d);if(e)return e;e=++Xe.count+"";var f=!("mouseenter"!=b&&"mouseleave"!=b||!a.addEventListener||"onmouseenter"in document);var g=f?function(d){d=new We(d);if(!Yc(d.relatedTarget,function(b){return b==a},!0))return d.currentTarget=a,d.type=b,c.call(a,d)}:function(b){b=new We(b);
b.currentTarget=a;return c.call(a,b)};
g=me(g);a.addEventListener?("mouseenter"==b&&f?b="mouseover":"mouseleave"==b&&f?b="mouseout":"mousewheel"==b&&"MozBoxSizing"in document.documentElement.style&&(b="MozMousePixelScroll"),a.addEventListener(b,g,d)):a.attachEvent("on"+b,g);bb[e]=[a,b,c,g,d];return e}
function Ze(a,b,c){var d=a||document;return T(d,"click",function(a){var e=Yc(a.target,function(a){return a===d||c(a)},!0);
e&&e!==d&&!e.disabled&&(a.currentTarget=e,b.call(e,a))})}
function $e(a){a=a||window.event;a=a.target||a.srcElement;3==a.nodeType&&(a=a.parentNode);return a}
function af(a,b,c){return Ze(a,b,function(a){return B(a,c)})}
function bf(a){if(document.createEvent){var b=document.createEvent("HTMLEvents");b.initEvent("click",!0,!0);a.dispatchEvent(b)}else b=document.createEventObject(),a.fireEvent("onclick",b)}
function U(a){a&&("string"==typeof a&&(a=[a]),x(a,function(a){if(a in bb){var b=bb[a],d=b[0],e=b[1],f=b[3];b=b[4];d.removeEventListener?d.removeEventListener(e,f,b):d.detachEvent&&d.detachEvent("on"+e,f);delete bb[a]}}))}
;var cf={},df="ontouchstart"in document;function ef(a,b,c){switch(a){case "mouseover":case "mouseout":var d=3;break;case "mouseenter":case "mouseleave":d=9}return Yc(c,function(a){return B(a,b)},!0,d)}
function V(a){var b="mouseover"==a.type&&"mouseenter"in cf||"mouseout"==a.type&&"mouseleave"in cf,c=a.type in cf||b;if("HTML"!=a.target.tagName&&c){if(b){b="mouseover"==a.type?"mouseenter":"mouseleave";c=cf[b];for(var d in c.f){var e=ef(b,d,a.target);e&&!Yc(a.relatedTarget,function(a){return a==e},!0)&&c.A(d,e,b,a)}}if(b=cf[a.type])for(d in b.f)(e=ef(a.type,d,a.target))&&b.A(d,e,a.type,a)}}
T(document,"blur",V,!0);T(document,"change",V,!0);T(document,"click",V);T(document,"focus",V,!0);T(document,"mouseover",V);T(document,"mouseout",V);T(document,"mousedown",V);T(document,"keydown",V);T(document,"keyup",V);T(document,"keypress",V);T(document,"cut",V);T(document,"paste",V);df&&(T(document,"touchstart",V),T(document,"touchend",V),T(document,"touchcancel",V));function W(a){this.j=a;this.o={};this.B=[];this.w=[]}
k=W.prototype;k.v=function(a){return J(a,X(this))};
function X(a,b){return"yt-uix"+(a.j?"-"+a.j:"")+(b?"-"+b:"")}
k.unregister=function(){ue(this.B);this.B.length=0;Ne(this.w);this.w.length=0};
k.init=oa;k.dispose=oa;function ff(a,b,c){a.B.push(se(b,c,a))}
function gf(a,b,c){a.w.push(Me(b,c,a))}
function Y(a,b,c,d){d=X(a,d);var e=r(c,a);b in cf||(cf[b]=new K);cf[b].subscribe(d,e);a.o[c]=e}
function Z(a,b,c,d){if(b in cf){var e=cf[b];Vd(e,X(a,d),a.o[c]);0>=Xd(e)&&(e.dispose(),delete cf[b])}delete a.o[c]}
k.G=function(a,b,c){var d=this.g(a,b);if(d&&(d=q(d))){var e=Ma(arguments,2);La(e,0,0,a);d.apply(null,e)}};
k.g=function(a,b){return ye(a,b)};
function hf(a,b){we(a,"tooltip-text",b)}
;var jf=window.yt&&window.yt.uix&&window.yt.uix.widgets_||{};t("yt.uix.widgets_",jf);za();var kf=la(XMLHttpRequest)?function(){return new XMLHttpRequest}:la(ActiveXObject)?function(){return new ActiveXObject("Microsoft.XMLHTTP")}:null;
function lf(){if(!kf)return null;var a=kf();return"open"in a?a:null}
;function mf(a){"?"==a.charAt(0)&&(a=a.substr(1));a=a.split("&");for(var b={},c=0,d=a.length;c<d;c++){var e=a[c].split("=");if(1==e.length&&e[0]||2==e.length){var f=decodeURIComponent((e[0]||"").replace(/\+/g," "));e=decodeURIComponent((e[1]||"").replace(/\+/g," "));f in b?"array"==qa(b[f])?Ka(b[f],e):b[f]=[b[f],e]:b[f]=e}}return b}
;var nf={"X-Goog-Visitor-Id":"SANDBOXED_VISITOR_ID","X-YouTube-Client-Name":"INNERTUBE_CONTEXT_CLIENT_NAME","X-YouTube-Client-Version":"INNERTUBE_CONTEXT_CLIENT_VERSION","X-Youtube-Identity-Token":"ID_TOKEN","X-YouTube-Page-CL":"PAGE_CL","X-YouTube-Page-Label":"PAGE_BUILD_LABEL","X-YouTube-Variants-Checksum":"VARIANTS_CHECKSUM"},of=!1;
function pf(a,b){b=void 0===b?{}:b;if(!c)var c=window.location.href;var d=a.match(ce)[1]||null,e=de(a.match(ce)[3]||null);d&&e?(d=c,c=a.match(ce),d=d.match(ce),c=c[3]==d[3]&&c[1]==d[1]&&c[4]==d[4]):c=e?de(c.match(ce)[3]||null)==e&&(Number(c.match(ce)[4]||null)||null)==(Number(a.match(ce)[4]||null)||null):!0;for(var f in nf){if((e=d=L(nf[f]))&&!(e=c)){e=f;var g=L("CORS_HEADER_WHITELIST")||{},h=de(a.match(ce)[3]||null);e=h?(g=g[h])?Ha(g,e):!1:!0}e&&(b[f]=d)}return b}
function qf(a,b){var c=b.format||"JSON";a=rf(a,b);var d=sf(a,b),e=!1,f,g=tf(a,function(a){if(!e){e=!0;f&&window.clearTimeout(f);a:switch(a&&"status"in a?a.status:-1){case 200:case 201:case 202:case 203:case 204:case 205:case 206:case 304:var d=!0;break a;default:d=!1}var g=null;if(d||400<=a.status&&500>a.status)g=uf(c,a,b.Hb);if(d)a:if(a&&204==a.status)d=!0;else{switch(c){case "XML":d=0==parseInt(g&&g.return_code,10);break a;case "RAW":d=!0;break a}d=!!g}g=g||{};var h=b.context||m;d?b.H&&b.H.call(h,
a,g):b.onError&&b.onError.call(h,a,g);b.V&&b.V.call(h,a,g)}},b.method,d,b.headers,b.responseType,b.withCredentials);
b.ob&&0<b.timeout&&(f=M(function(){e||(e=!0,g.abort(),window.clearTimeout(f),b.ob.call(b.context||m,g))},b.timeout))}
function rf(a,b){b.Jb&&(a=document.location.protocol+"//"+document.location.hostname+(document.location.port?":"+document.location.port:"")+a);var c=L("XSRF_FIELD_NAME",void 0),d=b.ya;if(d){d[c]&&delete d[c];d=d||{};var e=a.split("#",2);c=e[0];e=1<e.length?"#"+e[1]:"";var f=c.split("?",2);c=f[0];f=mf(f[1]||"");for(var g in d)f[g]=d[g];a=je(c,f)+e}return a}
function sf(a,b){var c=L("XSRF_FIELD_NAME",void 0),d=L("XSRF_TOKEN",void 0),e=b.postBody||"",f=b.D,g=L("XSRF_FIELD_NAME",void 0),h;b.headers&&(h=b.headers["Content-Type"]);b.Ib||de(a.match(ce)[3]||null)&&!b.withCredentials&&de(a.match(ce)[3]||null)!=document.location.hostname||"POST"!=b.method||h&&"application/x-www-form-urlencoded"!=h||b.D&&b.D[g]||(f||(f={}),f[c]=d);f&&p(e)&&(e=mf(e),db(e,f),e=b.rb&&"JSON"==b.rb?JSON.stringify(e):he(e));if(!(c=e)&&(c=f)){a:{for(l in f){var l=!1;break a}l=!0}c=!l}!of&&
c&&"POST"!=b.method&&(of=!0,ne(Error("AJAX request with postData should use POST")));return e}
function uf(a,b,c){var d=null;switch(a){case "JSON":a=b.responseText;b=b.getResponseHeader("Content-Type")||"";a&&0<=b.indexOf("json")&&(d=JSON.parse(a));break;case "XML":if(b=(b=b.responseXML)?vf(b):null)d={},x(b.getElementsByTagName("*"),function(a){d[a.tagName]=wf(a)})}c&&xf(d);
return d}
function xf(a){if(ta(a))for(var b in a){var c;(c="html_content"==b)||(c=b.length-5,c=0<=c&&b.indexOf("_html",c)==c);if(c){c=b;var d=uc(a[b]);a[c]=d}else xf(a[b])}}
function vf(a){return a?(a=("responseXML"in a?a.responseXML:a).getElementsByTagName("root"))&&0<a.length?a[0]:null:null}
function wf(a){var b="";x(a.childNodes,function(a){b+=a.nodeValue});
return b}
function tf(a,b,c,d,e,f,g){function h(){4==(l&&"readyState"in l?l.readyState:0)&&b&&me(b)(l)}
c=void 0===c?"GET":c;d=void 0===d?"":d;var l=lf();if(!l)return null;"onloadend"in l?l.addEventListener("loadend",h,!1):l.onreadystatechange=h;l.open(c,a,!0);f&&(l.responseType=f);g&&(l.withCredentials=!0);c="POST"==c&&(void 0===window.FormData||!(d instanceof FormData));if(e=pf(a,e))for(var n in e)l.setRequestHeader(n,e[n]),"content-type"==n.toLowerCase()&&(c=!1);c&&l.setRequestHeader("Content-Type","application/x-www-form-urlencoded");l.send(d);return l}
;function yf(a){Q.call(this,1,arguments);this.b=a}
u(yf,Q);function zf(a){Q.call(this,1,arguments);this.b=a}
u(zf,Q);function Af(a,b,c){Q.call(this,3,arguments);this.h=a;this.f=b;this.b=null!=c?!!c:null}
u(Af,Q);function Bf(a,b,c,d,e){Q.call(this,2,arguments);this.f=a;this.b=b;this.i=c||null;this.h=d||null;this.source=e||null}
u(Bf,Q);function Cf(a,b,c){Q.call(this,1,arguments);this.b=a;this.f=b}
u(Cf,Q);function Df(a,b,c,d,e,f,g){Q.call(this,1,arguments);this.f=a;this.j=b;this.b=c;this.l=d||null;this.i=e||null;this.h=f||null;this.source=g||null}
u(Df,Q);
var Ef=new R("subscription-batch-subscribe",yf),Ff=new R("subscription-batch-unsubscribe",yf),Gf=new R("subscription-subscribe",Bf),Hf=new R("subscription-subscribe-loading",zf),If=new R("subscription-subscribe-loaded",zf),Jf=new R("subscription-subscribe-success",Cf),Kf=new R("subscription-subscribe-external",Bf),Lf=new R("subscription-unsubscribe",Df),Mf=new R("subscription-unsubscirbe-loading",zf),Nf=new R("subscription-unsubscribe-loaded",zf),Of=new R("subscription-unsubscribe-success",zf),Pf=
new R("subscription-external-unsubscribe",Df),Qf=new R("subscription-enable-ypc",zf),Rf=new R("subscription-disable-ypc",zf),Sf=new R("subscription-prefs",Af),Tf=new R("subscription-prefs-success",Af),Uf=new R("subscription-prefs-failure",Af);function Vf(a){Q.call(this,1,arguments)}
u(Vf,Q);function Wf(a,b){Q.call(this,2,arguments);this.f=a;this.b=b}
u(Wf,Q);function Xf(a,b,c,d){Q.call(this,1,arguments);this.b=b;this.f=c||null;this.itemId=d||null}
u(Xf,Q);function Yf(a,b){Q.call(this,1,arguments);this.f=a;this.b=b||null}
u(Yf,Q);function Zf(a){Q.call(this,1,arguments)}
u(Zf,Q);var $f=new R("ypc-core-load",Vf),ag=new R("ypc-guide-sync-success",Wf),bg=new R("ypc-purchase-success",Xf),cg=new R("ypc-subscription-cancel",Zf),dg=new R("ypc-subscription-cancel-success",Yf),eg=new R("ypc-init-subscription",Zf);var fg=!1,gg=[];function hg(a){a.b?fg?S(Kf,a):S($f,new Vf(function(){S(eg,new Zf(a.b))})):ig(a.f,a.i,a.h,a.source)}
function jg(a){a.b?fg?S(Pf,a):S($f,new Vf(function(){S(cg,new Zf(a.b))})):kg(a.f,a.j,a.i,a.h,a.source)}
function lg(a){mg(Ja(a.b))}
function ng(a){og(Ja(a.b))}
function pg(a){qg(a.h,a.f,a.b)}
function rg(a){var b=a.itemId,c=a.b.subscriptionId;b&&c&&S(Jf,new Cf(b,c,a.b.channelInfo))}
function sg(a){var b=a.b;Za(a.f,function(a,d){S(Jf,new Cf(d,a,b[d]))})}
function tg(a){S(Of,new zf(a.f.itemId));a.b&&a.b.length&&(ug(a.b,Of),ug(a.b,Qf))}
function ig(a,b,c,d){var e=new zf(a);S(Hf,e);var f={};f.c=a;c&&(f.eurl=c);d&&(f.source=d);c={};(d=L("PLAYBACK_ID"))&&(c.plid=d);(d=L("EVENT_ID"))&&(c.ei=d);b&&vg(b,c);qf("/subscription_ajax?action_create_subscription_to_channel=1",{method:"POST",ya:f,D:c,H:function(b,c){var d=c.response;S(Jf,new Cf(a,d.id,d.channel_info));d.show_feed_privacy_dialog&&O("SHOW-FEED-PRIVACY-SUBSCRIBE-DIALOG",a)},
V:function(){S(If,e)}})}
function kg(a,b,c,d,e){var f=new zf(a);S(Mf,f);var g={};g.c=a;d&&(g.eurl=d);e&&(g.source=e);d={};d.c=a;d.s=b;(a=L("PLAYBACK_ID"))&&(d.plid=a);(a=L("EVENT_ID"))&&(d.ei=a);c&&vg(c,d);qf("/subscription_ajax?action_remove_subscriptions=1",{method:"POST",ya:g,D:d,H:function(){S(Of,f)},
V:function(){S(Nf,f)}})}
function qg(a,b,c){if(a){var d={};d.channel_id=a;switch(b){case "receive-all-updates":d.receive_all_updates=!0;break;case "receive-no-updates":d.receive_no_updates=!0;d.receive_post_updates=!1;break;case "receive-highlight-updates":d.receive_all_updates=!1;d.receive_no_updates=!1;break;default:return}null===c||d.receive_no_updates||(d.receive_post_updates=c);var e=new Af(a,b,c);qf("/subscription_ajax?action_update_subscription_preferences=1",{method:"POST",D:d,onError:function(){S(Uf,e)},
H:function(){S(Tf,e)}})}}
function mg(a){if(a.length){var b=La(a,0,40);S("subscription-batch-subscribe-loading");ug(b,Hf);var c={};c.a=b.join(",");var d=function(){S("subscription-batch-subscribe-loaded");ug(b,If)};
qf("/subscription_ajax?action_create_subscription_to_all=1",{method:"POST",D:c,H:function(c,f){d();var e=f.response,h=e.id;if("array"==qa(h)&&h.length==b.length){var l=e.channel_info_map;x(h,function(a,c){var d=b[c];S(Jf,new Cf(d,a,l[d]))});
a.length?mg(a):S("subscription-batch-subscribe-finished")}},
onError:function(){d();S("subscription-batch-subscribe-failure")}})}}
function og(a){if(a.length){var b=La(a,0,40);S("subscription-batch-unsubscribe-loading");ug(b,Mf);var c={};c.c=b.join(",");var d=function(){S("subscription-batch-unsubscribe-loaded");ug(b,Nf)};
qf("/subscription_ajax?action_remove_subscriptions=1",{method:"POST",D:c,H:function(){d();ug(b,Of);a.length&&og(a)},
onError:function(){d()}})}}
function ug(a,b){x(a,function(a){S(b,new zf(a))})}
function vg(a,b){var c=mf(a),d;for(d in c)b[d]=c[d]}
;function wg(){var a=Te();return a?a:null}
;function xg(a,b){(a=H(a))&&a.style&&(a.style.display=b?"":"none",F(a,"hid",!b))}
function yg(a){return(a=H(a))?"none"!=a.style.display&&!B(a,"hid"):!1}
function zg(a){x(arguments,function(a){!ra(a)||a instanceof Element?xg(a,!0):x(a,function(a){zg(a)})})}
function Ag(a){x(arguments,function(a){!ra(a)||a instanceof Element?xg(a,!1):x(a,function(a){Ag(a)})})}
;function Bg(){W.call(this,"tooltip");this.b=0;this.f={}}
u(Bg,W);pa(Bg);k=Bg.prototype;k.register=function(){Y(this,"mouseover",this.M);Y(this,"mouseout",this.C);Y(this,"focus",this.ha);Y(this,"blur",this.ca);Y(this,"click",this.C);Y(this,"touchstart",this.xa);Y(this,"touchend",this.O);Y(this,"touchcancel",this.O)};
k.unregister=function(){Z(this,"mouseover",this.M);Z(this,"mouseout",this.C);Z(this,"focus",this.ha);Z(this,"blur",this.ca);Z(this,"click",this.C);Z(this,"touchstart",this.xa);Z(this,"touchend",this.O);Z(this,"touchcancel",this.O);this.dispose();Bg.u.unregister.call(this)};
k.dispose=function(){for(var a in this.f)this.C(this.f[a]);this.f={}};
k.M=function(a){if(!(this.b&&1E3>za()-this.b)){var b=parseInt(this.g(a,"tooltip-hide-timer"),10);b&&(ze(a,"tooltip-hide-timer"),window.clearTimeout(b));b=r(function(){Cg(this,a);ze(a,"tooltip-show-timer")},this);
var c=parseInt(this.g(a,"tooltip-show-delay"),10)||0;b=M(b,c);we(a,"tooltip-show-timer",b.toString());a.title&&(hf(a,Dg(this,a)),a.title="");b=ua(a).toString();this.f[b]=a}};
k.C=function(a){var b=parseInt(this.g(a,"tooltip-show-timer"),10);b&&(window.clearTimeout(b),ze(a,"tooltip-show-timer"));b=r(function(){if(a){var b=H(Eg(this,a));b&&(Fg(b),Jc(b),ze(a,"content-id"));b=H(Eg(this,a,"arialabel"));Jc(b)}ze(a,"tooltip-hide-timer")},this);
b=M(b,50);we(a,"tooltip-hide-timer",b.toString());if(b=this.g(a,"tooltip-text"))a.title=b;b=ua(a).toString();delete this.f[b]};
k.ha=function(a,b){this.b=0;this.M(a,b)};
k.ca=function(a){this.b=0;this.C(a)};
k.xa=function(a,b,c){c.changedTouches&&(this.b=0,(a=ef(b,X(this),c.changedTouches[0].target))&&this.M(a,b))};
k.O=function(a,b,c){c.changedTouches&&(this.b=za(),(a=ef(b,X(this),c.changedTouches[0].target))&&this.C(a))};
function Gg(a,b,c){hf(b,c);a=a.g(b,"content-id");(a=H(a))&&Nc(a,c)}
function Dg(a,b){return a.g(b,"tooltip-text")||b.title}
function Cg(a,b){if(b){var c=Dg(a,b);if(c){var d=H(Eg(a,b));if(!d){d=document.createElement("div");d.id=Eg(a,b);d.className=X(a,"tip");var e=document.createElement("div");e.className=X(a,"tip-body");var f=document.createElement("div");f.className=X(a,"tip-arrow");var g=document.createElement("div");g.setAttribute("aria-hidden","true");g.className=X(a,"tip-content");var h=Hg(a,b),l=Eg(a,b,"content");g.id=l;we(b,"content-id",l);e.appendChild(g);h&&d.appendChild(h);d.appendChild(e);d.appendChild(f);
var n=Vc(b);l=Eg(a,b,"arialabel");f=document.createElement("div");C(f,X(a,"arialabel"));f.id=l;n=b.hasAttribute("aria-label")?b.getAttribute("aria-label"):"rtl"==document.body.getAttribute("dir")?c+" "+n:n+" "+c;Nc(f,n);b.setAttribute("aria-labelledby",l);l=wg()||document.body;l.appendChild(f);l.appendChild(d);Gg(a,b,c);(c=parseInt(a.g(b,"tooltip-max-width"),10))&&e.offsetWidth>c&&(e.style.width=c+"px",C(g,X(a,"normal-wrap")));g=B(b,X(a,"reverse"));Ig(a,b,d,e,h,g)||Ig(a,b,d,e,h,!g);var w=X(a,"tip-visible");
M(function(){C(d,w)},0)}}}}
function Ig(a,b,c,d,e,f){F(c,X(a,"tip-reverse"),f);var g=0;f&&(g=1);var h=qd(b);f=new G((h.width-10)/2,f?h.height:0);var l=nd(b);Dd(new G(l.x+f.x,l.y+f.y),c,g);f=Ec(window);if(1==c.nodeType)var n=od(c);else c=c.changedTouches?c.changedTouches[0]:c,n=new G(c.clientX,c.clientY);c=qd(d);var w=Math.floor(c.width/2);g=!!(f.height<n.y+h.height);h=!!(n.y<h.height);l=!!(n.x<w);f=!!(f.width<n.x+w);n=(c.width+3)/-2- -5;a=a.g(b,"force-tooltip-direction");if("left"==a||l)n=-5;else if("right"==a||f)n=20-c.width-
3;a=Math.floor(n)+"px";d.style.left=a;e&&(e.style.left=a,e.style.height=c.height+"px",e.style.width=c.width+"px");return!(g||h)}
function Eg(a,b,c){a=X(a)+Pe(b);c&&(a+="-"+c);return a}
function Hg(a,b){var c=null;Kb&&B(b,X(a,"masked"))&&((c=H("yt-uix-tooltip-shared-mask"))?(c.parentNode.removeChild(c),zg(c)):(c=document.createElement("IFRAME"),c.src='javascript:""',c.id="yt-uix-tooltip-shared-mask",c.className=X(a,"tip-mask")));return c}
function Fg(a){var b=H("yt-uix-tooltip-shared-mask"),c=b&&Yc(b,function(b){return b==a},!1,2);
b&&c&&(b.parentNode.removeChild(b),Ag(b),document.body.appendChild(b))}
;function Jg(a){var b=Kg();if(b=window.open(b,"loginPopup","width=375,height=440,resizable=yes,scrollbars=yes",!0)){var c=se("LOGGED_IN",function(b){ue(L("LOGGED_IN_PUBSUB_KEY",void 0));le("LOGGED_IN",!0);a(b)});
le("LOGGED_IN_PUBSUB_KEY",c);b.moveTo((screen.width-375)/2,(screen.height-440)/2)}}
function Kg(){var a=document.location.protocol+"//"+document.domain+"/post_login";a=ie(a,"mode","subscribe");a=ie("/signin?context=popup","next",a);return a=ie(a,"feature","sub_button")}
t("yt.pubsub.publish",O);var Lg=Object.create(null);Lg.log_event="GENERIC_EVENT_LOGGING";Lg.log_interaction="INTERACTION_LOGGING";t("ytLoggingTransportLogPayloadsQueue_",q("ytLoggingTransportLogPayloadsQueue_")||{});t("ytLoggingTransportTokensToCttTargetIds_",q("ytLoggingTransportTokensToCttTargetIds_")||{});t("ytLoggingTransportDispatchedStats_",q("ytLoggingTransportDispatchedStats_")||{});t("ytytLoggingTransportCapturedTime_",q("ytLoggingTransportCapturedTime_")||{});var Mg=za().toString();var Ng;a:{if(window.crypto&&window.crypto.getRandomValues)try{var Og=Array(16),Pg=new Uint8Array(16);window.crypto.getRandomValues(Pg);for(var Qg=0;Qg<Og.length;Qg++)Og[Qg]=Pg[Qg];Ng=Og;break a}catch(a){}for(var Rg=Array(16),Sg=0;16>Sg;Sg++){for(var Tg=za(),Ug=0;Ug<Tg%23;Ug++)Rg[Sg]=Math.random();Rg[Sg]=Math.floor(256*Math.random())}if(Mg)for(var Vg=1,Wg=0;Wg<Mg.length;Wg++)Rg[Vg%16]=Rg[Vg%16]^Rg[(Vg-1)%16]/4^Mg.charCodeAt(Wg),Vg++;Ng=Rg}
for(var Xg=Ng,Yg=[],Zg=0;Zg<Xg.length;Zg++)Yg.push("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_".charAt(Xg[Zg]&63));(new ae).isAvailable();(new be).isAvailable();function $g(){W.call(this,"button");this.b=null;this.h=[];this.f={}}
u($g,W);pa($g);k=$g.prototype;k.register=function(){Y(this,"click",this.Ba);Y(this,"keydown",this.la);Y(this,"keypress",this.ma);ff(this,"page-scroll",this.ab)};
k.unregister=function(){Z(this,"click",this.Ba);Z(this,"keydown",this.la);Z(this,"keypress",this.ma);ah(this);this.f={};$g.u.unregister.call(this)};
k.Ba=function(a){a&&!a.disabled&&(this.toggle(a),this.click(a))};
k.la=function(a,b,c){if(!(c.altKey||c.ctrlKey||c.shiftKey||c.metaKey)&&(b=bh(this,a))){var d=function(a){var b="";a.tagName&&(b=a.tagName.toLowerCase());return"ul"==b||"table"==b},e;
d(b)?e=b:e=Oc(b,d);if(e){e=e.tagName.toLowerCase();if("ul"==e)var f=this.ib;else"table"==e&&(f=this.hb);f&&ch(this,a,b,c,r(f,this))}}};
k.ab=function(){var a=this.f,b=0;for(d in a)b++;if(0!=b)for(var c in a){b=a[c];var d=J(b.activeButtonNode||b.parentNode,X(this));if(void 0==d||void 0==b)break;dh(this,d,b,!0)}};
function ch(a,b,c,d,e){var f=yg(c),g=9==d.keyCode;if(g||32==d.keyCode||13==d.keyCode)if(d=eh(a,c)){if(la(d.firstElementChild))b=d.firstElementChild;else for(b=d.firstChild;b&&1!=b.nodeType;)b=b.nextSibling;if("a"==b.tagName.toLowerCase()){var h=void 0===h?{}:h;var l=void 0===l?"":l;var n=void 0===n?window:n;n=n.location;h=je(b.href,h)+l;h instanceof nc||h instanceof nc||(h=h.T?h.S():String(h),pc.test(h)||(h="about:invalid#zClosurez"),h=qc(h));h instanceof nc&&h.constructor===nc&&h.f===oc?h=h.b:(qa(h),
h="type_error:SafeUrl");n.href=h}else bf(b)}else g&&fh(a,b);else f?27==d.keyCode?(eh(a,c),fh(a,b)):e(b,c,d):(h=B(b,X(a,"reverse"))?38:40,d.keyCode==h&&(bf(b),d.preventDefault()))}
k.ma=function(a,b,c){c.altKey||c.ctrlKey||c.shiftKey||c.metaKey||(a=bh(this,a),yg(a)&&c.preventDefault())};
function eh(a,b){var c=X(a,"menu-item-highlight"),d=I(c,b);d&&D(d,c);return d}
function gh(a,b,c){C(c,X(a,"menu-item-highlight"));var d=c.getAttribute("id");d||(d=X(a,"item-id-"+ua(c)),c.setAttribute("id",d));b.setAttribute("aria-activedescendant",d)}
k.hb=function(a,b,c){var d=eh(this,b);if(d){var e=Se("table",b);b=Bc(document,"td",null,e);d=hh(d,b,Bc(document,"td",null,Se("tr",e)).length,c);-1!=d&&(gh(this,a,b[d]),c.preventDefault())}};
k.ib=function(a,b,c){if(40==c.keyCode||38==c.keyCode){var d=eh(this,b);d&&(b=Da(Bc(document,"li",null,b),yg),gh(this,a,b[hh(d,b,1,c)]),c.preventDefault())}};
function hh(a,b,c,d){var e=b.length;a=Ca(b,a);if(-1==a)if(38==d.keyCode)a=e-c;else{if(37==d.keyCode||38==d.keyCode||40==d.keyCode)a=0}else 39==d.keyCode?(a%c==c-1&&(a-=c),a+=1):37==d.keyCode?(0==a%c&&(a+=c),--a):38==d.keyCode?(a<c&&(a+=e),a-=c):40==d.keyCode&&(a>=e-c&&(a-=e),a+=c);return a}
function ih(a,b){var c=b.iframeMask;c||(c=document.createElement("IFRAME"),c.src='javascript:""',c.className=X(a,"menu-mask"),Ag(c),b.iframeMask=c);return c}
function dh(a,b,c,d){var e=J(b,X(a,"group")),f=!!a.g(b,"button-menu-ignore-group");e=e&&!f?e:b;f=9;var g=8,h=sd(b);if(B(b,X(a,"reverse"))){f=8;g=9;h=h.top+"px";try{c.style.maxHeight=h}catch(w){}}B(b,"flip")&&(B(b,X(a,"reverse"))?(f=12,g=13):(f=13,g=12));var l;a.g(b,"button-has-sibling-menu")?l=ld(e):a.g(b,"button-menu-root-container")&&(l=jh(a,b));A&&!Wb("8")&&(l=null);if(l){var n=sd(l);n=new dd(-n.top,n.left,n.top,-n.left)}l=new G(0,1);B(b,X(a,"center-menu"))&&(l.x-=Math.round((qd(c).width-qd(b).width)/
2));d&&(l.y+=Gc(document).y);if(a=ih(a,b))b=qd(c),a.style.width=b.width+"px",a.style.height=b.height+"px",Bd(e,f,a,g,l,n,197),d&&fd(a,"position","fixed");Bd(e,f,c,g,l,n,197)}
function jh(a,b){if(a.g(b,"button-menu-root-container")){var c=a.g(b,"button-menu-root-container");return J(b,c)}return document.body}
k.Da=function(a){if(a){var b=bh(this,a);if(b){a.setAttribute("aria-pressed","true");a.setAttribute("aria-expanded","true");b.originalParentNode=b.parentNode;b.activeButtonNode=a;b.parentNode.removeChild(b);var c;this.g(a,"button-has-sibling-menu")?c=a.parentNode:c=jh(this,a);c.appendChild(b);b.style.minWidth=a.offsetWidth-2+"px";var d=ih(this,a);d&&c.appendChild(d);(c=!!this.g(a,"button-menu-fixed"))&&(this.f[Pe(a).toString()]=b);dh(this,a,b,c);ve("yt-uix-button-menu-before-show",a,b);zg(b);d&&zg(d);
this.G(a,"button-menu-action",!0);C(a,X(this,"active"));b=r(this.Ca,this,a,!1);d=r(this.Ca,this,a,!0);c=r(this.tb,this,a,void 0);this.b&&bh(this,this.b)==bh(this,a)||ah(this);O("yt-uix-button-menu-show",a);U(this.h);this.h=[T(document,"click",d),T(document,"contextmenu",b),T(window,"resize",c)];this.b=a}}};
function fh(a,b){if(b){var c=bh(a,b);if(c){a.b=null;b.setAttribute("aria-pressed","false");b.setAttribute("aria-expanded","false");b.removeAttribute("aria-activedescendant");Ag(c);a.G(b,"button-menu-action",!1);var d=ih(a,b),e=Pe(c).toString();delete a.f[e];M(function(){d&&d.parentNode&&(Ag(d),d.parentNode.removeChild(d));c.originalParentNode&&(c.parentNode.removeChild(c),c.originalParentNode.appendChild(c),c.originalParentNode=null,c.activeButtonNode=null)},1)}e=J(b,X(a,"group"));
var f=[X(a,"active")];e&&f.push(X(a,"group-active"));cc(b,f);O("yt-uix-button-menu-hide",b);U(a.h);a.h.length=0}}
k.tb=function(a,b){var c=bh(this,a);if(c){b&&(b instanceof rc?c.innerHTML=tc(b):Nc(c,b));var d=!!this.g(a,"button-menu-fixed");dh(this,a,c,d)}};
k.Ca=function(a,b,c){c=$e(c);var d=J(c,X(this));if(d){d=bh(this,d);var e=bh(this,a);if(d==e)return}d=J(c,X(this,"menu"));e=d==bh(this,a);var f=B(c,X(this,"menu-item")),g=B(c,X(this,"menu-close"));if(!d||e&&(f||g))fh(this,a),d&&b&&this.g(a,"button-menu-indicate-selected")&&((a=I(X(this,"content"),a))&&Nc(a,Vc(c)),kh(this,d,c))};
function kh(a,b,c){var d=X(a,"menu-item-selected");x(Ac(d,b),function(a){D(a,d)});
C(c.parentNode,d)}
function bh(a,b){if(!b.widgetMenu){var c=a.g(b,"button-menu-id");c=c&&H(c);var d=X(a,"menu");c?bc(c,[d,X(a,"menu-external")]):c=I(d,b);b.widgetMenu=c}return b.widgetMenu}
k.isToggled=function(a){return B(a,X(this,"toggled"))};
k.toggle=function(a){if(this.g(a,"button-toggle")){var b=J(a,X(this,"group")),c=X(this,"toggled"),d=B(a,c);if(b&&this.g(b,"button-toggle-group")){var e=this.g(b,"button-toggle-group");x(Ac(X(this),b),function(b){b!=a||"optional"==e&&d?(D(b,c),b.removeAttribute("aria-pressed")):(C(a,c),b.setAttribute("aria-pressed","true"))})}else d?a.removeAttribute("aria-pressed"):a.setAttribute("aria-pressed","true"),ec(a,c)}};
k.click=function(a){if(bh(this,a)){var b=bh(this,a);if(b){var c=J(b.activeButtonNode||b.parentNode,X(this));c&&c!=a?(fh(this,c),M(r(this.Da,this,a),1)):yg(b)?fh(this,a):this.Da(a)}a.focus()}this.G(a,"button-action")};
function ah(a){a.b&&fh(a,a.b)}
;function lh(a){W.call(this,a);this.h=null}
u(lh,W);k=lh.prototype;k.v=function(a){var b=W.prototype.v.call(this,a);return b?b:a};
k.register=function(){ff(this,"yt-uix-kbd-nav-move-out-done",this.hide)};
k.dispose=function(){mh(this);lh.u.dispose.call(this)};
k.g=function(a,b){var c=lh.u.g.call(this,a,b);return c?c:(c=lh.u.g.call(this,a,"card-config"))&&(c=q(c))&&c[b]?c[b]:null};
k.show=function(a){var b=this.v(a);if(b){C(b,X(this,"active"));var c=nh(this,a,b);if(c){c.cardTargetNode=a;c.cardRootNode=b;oh(this,a,c);var d=X(this,"card-visible"),e=this.g(a,"card-delegate-show")&&this.g(b,"card-action");this.G(b,"card-action",a);this.h=a;Ag(c);M(r(function(){e||(zg(c),O("yt-uix-card-show",b,a,c));ph(c);C(c,d);O("yt-uix-kbd-nav-move-in-to",c)},this),10)}}};
function nh(a,b,c){var d=c||b,e=X(a,"card");c=qh(a,d);var f=H(X(a,"card")+Pe(d));if(f)return a=I(X(a,"card-body"),f),Mc(a,c)||(Jc(c),a.appendChild(c)),f;f=document.createElement("div");f.id=X(a,"card")+Pe(d);f.className=e;(d=a.g(d,"card-class"))&&bc(f,d.split(/\s+/));d=document.createElement("div");d.className=X(a,"card-border");b=a.g(b,"orientation")||"horizontal";e=document.createElement("div");e.className="yt-uix-card-border-arrow yt-uix-card-border-arrow-"+b;var g=document.createElement("div");
g.className=X(a,"card-body");a=document.createElement("div");a.className="yt-uix-card-body-arrow yt-uix-card-body-arrow-"+b;Jc(c);g.appendChild(c);d.appendChild(a);d.appendChild(g);f.appendChild(e);f.appendChild(d);document.body.appendChild(f);return f}
function oh(a,b,c){var d=a.g(b,"orientation")||"horizontal",e=I(X(a,"anchor"),b)||b,f=a.g(b,"position"),g=!!a.g(b,"force-position"),h=a.g(b,"position-fixed");d="horizontal"==d;var l="bottomright"==f||"bottomleft"==f,n="topright"==f||"bottomright"==f;if(n&&l){var w=13;var E=8}else n&&!l?(w=12,E=9):!n&&l?(w=9,E=12):(w=8,E=13);var v=td(document.body);f=td(b);v!=f&&(w^=4);if(d){f=b.offsetHeight/2-12;var P=new G(-12,b.offsetHeight+6)}else f=b.offsetWidth/2-6,P=new G(b.offsetWidth+6,-12);var N=qd(c);f=
Math.min(f,(d?N.height:N.width)-24-6);6>f&&(f=6,d?P.y+=12-b.offsetHeight/2:P.x+=12-b.offsetWidth/2);N=null;g||(N=10);b=X(a,"card-flip");a=X(a,"card-reverse");F(c,b,n);F(c,a,l);N=Bd(e,w,c,E,P,null,N);!g&&N&&(N&48&&(n=!n,w^=4,E^=4),N&192&&(l=!l,w^=1,E^=1),F(c,b,n),F(c,a,l),Bd(e,w,c,E,P));h&&(e=parseInt(c.style.top,10),g=Gc(document).y,fd(c,"position","fixed"),fd(c,"top",e-g+"px"));v&&(c.style.right="",e=sd(c),e.left=e.left||parseInt(c.style.left,10),g=Ec(window),c.style.left="",c.style.right=g.width-
e.left-e.width+"px");e=I("yt-uix-card-body-arrow",c);g=I("yt-uix-card-border-arrow",c);d=d?l?"top":"bottom":!v&&n||v&&!n?"left":"right";e.setAttribute("style","");g.setAttribute("style","");e.style[d]=f+"px";g.style[d]=f+"px";l=I("yt-uix-card-arrow",c);n=I("yt-uix-card-arrow-background",c);l&&n&&(c="right"==d?qd(c).width-f-13:f+11,f=c/Math.sqrt(2),l.style.left=c+"px",l.style.marginLeft="1px",n.style.marginLeft=-f+"px",n.style.marginTop=f+"px")}
k.hide=function(a){if(a=this.v(a)){var b=H(X(this,"card")+Pe(a));b&&(D(a,X(this,"active")),D(b,X(this,"card-visible")),Ag(b),this.h=null,b.cardTargetNode=null,b.cardRootNode=null,b.cardMask&&(Jc(b.cardMask),b.cardMask=null))}};
function mh(a){a.h&&a.hide(a.h)}
k.sb=function(a,b){var c=this.v(a);if(c){if(b){var d=qh(this,c);if(!d)return;b instanceof rc?d.innerHTML=tc(b):Nc(d,b)}B(c,X(this,"active"))&&(c=nh(this,a,c),oh(this,a,c),zg(c),ph(c))}};
k.isActive=function(a){return(a=this.v(a))?B(a,X(this,"active")):!1};
function qh(a,b){var c=b.cardContentNode;if(!c){var d=X(a,"content"),e=X(a,"card-content");(c=(c=a.g(b,"card-id"))?H(c):I(d,b))||(c=document.createElement("div"));var f=c;D(f,d);C(f,e);b.cardContentNode=c}return c}
function ph(a){var b=a.cardMask;b||(b=document.createElement("IFRAME"),b.src='javascript:""',bc(b,["yt-uix-card-iframe-mask"]),a.cardMask=b);b.style.position=a.style.position;b.style.top=a.style.top;b.style.left=a.offsetLeft+"px";b.style.height=a.clientHeight+"px";b.style.width=a.clientWidth+"px";document.body.appendChild(b)}
;function rh(){W.call(this,"kbd-nav")}
var sh;u(rh,W);pa(rh);k=rh.prototype;k.register=function(){Y(this,"keydown",this.ja);ff(this,"yt-uix-kbd-nav-move-in",this.ra);ff(this,"yt-uix-kbd-nav-move-in-to",this.jb);ff(this,"yt-uix-kbd-move-next",this.sa);ff(this,"yt-uix-kbd-nav-move-to",this.K)};
k.unregister=function(){Z(this,"keydown",this.ja);U(sh)};
k.ja=function(a,b,c){var d=c.keyCode;if(a=J(a,X(this)))switch(d){case 13:case 32:this.ra(a);break;case 27:c.preventDefault();c.stopImmediatePropagation();a:{for(c=mc(a,"kbdNavMoveOut");!c;){c=J(a.parentElement,X(this));if(!c)break a;c=mc(c,"kbdNavMoveOut")}c=H(c);this.K(c);O("yt-uix-kbd-nav-move-out-done",c)}break;case 40:case 38:if((b=c.target)&&B(a,X(this,"list")))switch(d){case 40:this.sa(b,a);break;case 38:d=document.activeElement==a,a=th(a),b=a.indexOf(b),0>b&&!d||(b=d?a.length-1:(a.length+b-
1)%a.length,a[b].focus(),uh(this,a[b]))}c.preventDefault()}};
k.ra=function(a){var b=mc(a,"kbdNavMoveIn");b=H(b);vh(this,a,b);this.K(b)};
k.jb=function(a){a:{var b=document;try{var c=b&&b.activeElement;break a}catch(d){}c=null}vh(this,c,a);this.K(a)};
k.K=function(a){if(a)if(Sc(a))a.focus();else{var b=Oc(a,function(a){return Lc(a)?Sc(a):!1});
b?b.focus():(a.setAttribute("tabindex","-1"),a.focus())}};
function vh(a,b,c){if(b&&c)if(C(c,X(a)),a=b.id,a||(a="kbd-nav-"+Math.floor(1E6*Math.random()+1),b.id=a),b=a,lc&&c.dataset)c.dataset.kbdNavMoveOut=b;else{if(/-[a-z]/.test("kbdNavMoveOut"))throw Error("");c.setAttribute("data-"+"kbdNavMoveOut".replace(/([A-Z])/g,"-$1").toLowerCase(),b)}}
k.sa=function(a,b){var c=document.activeElement==b,d=th(b),e=d.indexOf(a);0>e&&!c||(c=c?0:(e+1)%d.length,d[c].focus(),uh(this,d[c]))};
function uh(a,b){if(b){var c=Xc(b,"LI");c&&(C(c,X(a,"highlight")),sh=T(b,"blur",r(function(a){D(a,X(this,"highlight"));U(sh)},a,c)))}}
function th(a){if("UL"!=a.tagName.toUpperCase())return[];a=Da(Kc(a),function(a){return"LI"==a.tagName.toUpperCase()});
return Da(Ea(a,function(a){return yg(a)?Oc(a,function(a){return Lc(a)?Sc(a):!1}):!1}),function(a){return!!a})}
;function wh(){W.call(this,"menu");this.f=this.b=null;this.h={};this.l={};this.i=null}
u(wh,W);pa(wh);function xh(a){var b=wh.m();if(B(a,X(b)))return a;var c=b.v(a);return c?c:J(a,X(b,"content"))==b.b?b.f:null}
k=wh.prototype;k.register=function(){Y(this,"click",this.ia);Y(this,"mouseenter",this.Ya);ff(this,"page-scroll",this.bb);ff(this,"yt-uix-kbd-nav-move-out-done",function(a){a=this.v(a);yh(this,a)});
this.i=new K};
k.unregister=function(){Z(this,"click",this.ia);this.f=this.b=null;U(Na($a(this.h)));this.h={};Za(this.l,function(a){Jc(a)},this);
this.l={};yb(this.i);this.i=null;wh.u.unregister.call(this)};
k.ia=function(a,b,c){a&&(b=zh(this,a),!b.disabled&&Re(c.target,b)&&Ah(this,a))};
k.Ya=function(a,b,c){a&&B(a,X(this,"hover"))&&Re(c.target,zh(this,a))&&Ah(this,a,!0)};
k.bb=function(){this.b&&this.f&&Bh(this,this.f,this.b)};
function Bh(a,b,c){var d=Ch(a,b);if(d){var e=qd(c);if(e instanceof wc){var f=e.height;e=e.width}else throw Error("missing height argument");d.style.width=pd(e,!0);d.style.height=pd(f,!0)}c==a.b&&(e=9,f=8,B(b,X(a,"reversed"))&&(e^=1,f^=1),B(b,X(a,"flipped"))&&(e^=4,f^=4),a=new G(0,1),d&&Bd(b,e,d,f,a,null,197),Bd(b,e,c,f,a,null,197))}
function Ah(a,b,c){Dh(a,b)&&!c?yh(a,b):(Eh(a,b),!a.b||Re(b,a.b)?a.Ea(b):Ud(a.i,r(a.Ea,a,b)))}
k.Ea=function(a){if(a){var b=Fh(this,a);if(b){ve("yt-uix-menu-before-show",a,b);this.b?Re(a,this.b)||yh(this,this.f):(this.f=a,this.b=b,B(a,X(this,"sibling-content"))||(Jc(b),document.body.appendChild(b)),b.style.minWidth=zh(this,a).offsetWidth-2+"px");var c=Ch(this,a);c&&b.parentNode&&b.parentNode.insertBefore(c,b.nextSibling);D(b,X(this,"content-hidden"));Bh(this,a,b);bc(zh(this,a),[X(this,"trigger-selected"),"yt-uix-button-toggled"]);O("yt-uix-menu-show",a);Gh(b);Hh(this,a);O("yt-uix-kbd-nav-move-in-to",
b);var d=r(this.vb,this,a),e=r(this.fb,this,a);c=ua(a).toString();this.h[c]=[T(b,"click",e),T(document,"click",d)];B(a,X(this,"indicate-selected"))&&(d=r(this.gb,this,a),this.h[c].push(T(b,"click",d)));B(a,X(this,"hover"))&&(a=r(this.Za,this,a),this.h[c].push(T(document,"mousemove",a)))}}};
k.Za=function(a,b){var c=$e(b);c&&(Re(c,zh(this,a))||Ih(this,c)||Jh(this,a))};
k.vb=function(a,b){var c=$e(b);if(c){if(Ih(this,c)){var d=J(c,X(this,"content")),e=Xc(c,"LI");e&&d&&Mc(d,e)&&ve("yt-uix-menu-item-clicked",c);c=J(c,X(this,"close-on-select"));if(!c)return;d=xh(c)}yh(this,d||a)}};
function Eh(a,b){if(b){var c=J(b,X(a,"content"));c&&x(Ac(X(a),c),function(a){!Re(a,b)&&Dh(this,a)&&Jh(this,a)},a)}}
function yh(a,b){if(b){var c=[];c.push(b);var d=Fh(a,b);d&&(d=Ac(X(a),d),d=Ja(d),c=c.concat(d),x(c,function(a){Dh(this,a)&&Jh(this,a)},a))}}
function Jh(a,b){if(b){var c=Fh(a,b);cc(zh(a,b),[X(a,"trigger-selected"),"yt-uix-button-toggled"]);C(c,X(a,"content-hidden"));var d=Fh(a,b);d&&Cc(d,{"aria-expanded":"false"});(d=Ch(a,b))&&d.parentNode&&Jc(d);c&&c==a.b&&(a.f.appendChild(c),a.b=null,a.f=null,a.i&&a.i.A("ROOT_MENU_REMOVED"));O("yt-uix-menu-hide",b);c=ua(b).toString();U(a.h[c]);delete a.h[c]}}
k.fb=function(a,b){var c=$e(b);c&&Kh(this,a,c)};
k.gb=function(a,b){var c=$e(b);if(c){var d=zh(this,a);if(d&&(c=Xc(c,"LI")))if(c=Vc(c).trim(),d.hasChildNodes()){var e=$g.m();(d=I(X(e,"content"),d))&&Nc(d,c)}else Nc(d,c)}};
function Hh(a,b){var c=Fh(a,b);if(c){x(c.children,function(a){"LI"==a.tagName&&Cc(a,{role:"menuitem"})});
Cc(c,{"aria-expanded":"true"});var d=c.id;d||(d="aria-menu-id-"+ua(c),c.id=d);(c=zh(a,b))&&Cc(c,{"aria-controls":d})}}
function Kh(a,b,c){var d=Fh(a,b);d&&B(b,X(a,"checked"))&&(a=Xc(c,"LI"))&&(a=I("yt-ui-menu-item-checked-hid",a))&&(x(Ac("yt-ui-menu-item-checked",d),function(a){dc(a,"yt-ui-menu-item-checked","yt-ui-menu-item-checked-hid")}),dc(a,"yt-ui-menu-item-checked-hid","yt-ui-menu-item-checked"))}
function Dh(a,b){var c=Fh(a,b);return c?!B(c,X(a,"content-hidden")):!1}
function Gh(a){x(Bc(document,"UL",null,a),function(a){a.tabIndex=0;var b=rh.m();bc(a,[X(b),X(b,"list")])})}
function Fh(a,b){var c=ye(b,"menu-content-id");return c&&(c=H(c))?(bc(c,[X(a,"content"),X(a,"content-external")]),c):b==a.f?a.b:I(X(a,"content"),b)}
function Ch(a,b){var c=ua(b).toString(),d=a.l[c];if(!d){d=document.createElement("IFRAME");d.src='javascript:""';var e=[X(a,"mask")];x(ac(b),function(a){e.push(a+"-mask")});
bc(d,e);a.l[c]=d}return d||null}
function zh(a,b){return I(X(a,"trigger"),b)}
function Ih(a,b){return Re(b,a.b)||Re(b,a.f)}
;function Lh(){lh.call(this,"clickcard");this.b={};this.f={}}
u(Lh,lh);pa(Lh);k=Lh.prototype;k.register=function(){Lh.u.register.call(this);Y(this,"click",this.fa,"target");Y(this,"click",this.ea,"close")};
k.unregister=function(){Lh.u.unregister.call(this);Z(this,"click",this.fa,"target");Z(this,"click",this.ea,"close");for(var a in this.b)U(this.b[a]);this.b={};for(a in this.f)U(this.f[a]);this.f={}};
k.fa=function(a,b,c){c.preventDefault();b=Xc(c.target,"button");if(!b||!b.disabled){if(b=this.g(a,"card-target"))a=document,a=p(b)?a.getElementById(b):b;b=this.v(a);this.g(b,"disabled")||(B(b,X(this,"active"))?(this.hide(a),D(b,X(this,"active"))):(this.show(a),C(b,X(this,"active"))))}};
k.show=function(a){Lh.u.show.call(this,a);var b=this.v(a),c=ua(a).toString();if(!ye(b,"click-outside-persists")){if(this.b[c])return;b=T(document,"click",r(this.ga,this,a));var d=T(window,"blur",r(this.ga,this,a));this.b[c]=[b,d]}a=T(window,"resize",r(this.sb,this,a,void 0));this.f[c]=a};
k.hide=function(a){Lh.u.hide.call(this,a);a=ua(a).toString();var b=this.b[a];b&&(U(b),this.b[a]=null);if(b=this.f[a])U(b),delete this.f[a]};
k.ga=function(a,b){var c="yt-uix"+(this.j?"-"+this.j:"")+"-card",d=null;b.target&&(d=J(b.target,c)||J(xh(b.target),c));(d=d||J(document.activeElement,c)||J(xh(document.activeElement),c))||this.hide(a)};
k.ea=function(a){(a=J(a,X(this,"card")))&&(a=a.cardTargetNode)&&this.hide(a)};function Mh(){lh.call(this,"hovercard")}
u(Mh,lh);pa(Mh);k=Mh.prototype;k.register=function(){Y(this,"mouseenter",this.na,"target");Y(this,"mouseleave",this.pa,"target");Y(this,"mouseenter",this.oa,"card");Y(this,"mouseleave",this.qa,"card")};
k.unregister=function(){Z(this,"mouseenter",this.na,"target");Z(this,"mouseleave",this.pa,"target");Z(this,"mouseenter",this.oa,"card");Z(this,"mouseleave",this.qa,"card")};
k.na=function(a){if(Nh!=a){Nh&&(this.hide(Nh),Nh=null);var b=r(this.show,this,a),c=parseInt(this.g(a,"delay-show"),10);b=M(b,-1<c?c:200);we(a,"card-timer",b.toString());Nh=a;a.alt&&(we(a,"card-alt",a.alt),a.alt="");a.title&&(we(a,"card-title",a.title),a.title="")}};
k.pa=function(a){var b=parseInt(this.g(a,"card-timer"),10);window.clearTimeout(b);this.v(a).isCardHidable=!0;b=parseInt(this.g(a,"delay-hide"),10);b=-1<b?b:200;M(r(this.cb,this,a),b);if(b=this.g(a,"card-alt"))a.alt=b;if(b=this.g(a,"card-title"))a.title=b};
k.cb=function(a){this.v(a).isCardHidable&&(this.hide(a),Nh=null)};
k.oa=function(a){a&&(a.cardRootNode.isCardHidable=!1)};
k.qa=function(a){a&&this.hide(a.cardTargetNode)};
var Nh=null;function Oh(a,b,c,d,e,f){this.b=a;this.o=null;this.h=I("yt-dialog-fg",this.b)||this.b;if(a=I("yt-dialog-title",this.h)){var g="yt-dialog-title-"+ua(this.h);a.setAttribute("id",g);this.h.setAttribute("aria-labelledby",g)}this.h.setAttribute("tabindex","-1");this.P=I("yt-dialog-focus-trap",this.b);this.za=!1;this.i=new K;this.w=[];this.w.push(af(this.b,r(this.kb,this),"yt-dialog-dismiss"));this.w.push(T(this.P,"focus",r(this.Xa,this),!0));Ph(this);this.ub=b;this.Ga=c;this.Fa=d;this.B=e;this.Ja=f;this.l=
this.j=null}
var Qh={LOADING:"loading",yb:"content",Fb:"working"};function Rh(a,b){a.L()||a.i.subscribe("post-all",b)}
function Ph(a){a=I("yt-dialog-fg-content",a.b);var b=[];Za(Qh,function(a){b.push("yt-dialog-show-"+a)});
cc(a,b);C(a,"yt-dialog-show-content")}
k=Oh.prototype;
k.show=function(){if(!this.L()){this.o=document.activeElement;if(!this.Fa){this.f||(this.f=H("yt-dialog-bg"),this.f||(this.f=document.createElement("div"),this.f.id="yt-dialog-bg",this.f.className="yt-dialog-bg",document.body.appendChild(this.f)));var a=window,b=a.document;var c=0;if(b){c=b.body;var d=b.documentElement;if(d&&c)if(a=Ec(a).height,Fc(b)&&d.scrollHeight)c=d.scrollHeight!=a?d.scrollHeight:d.offsetHeight;else{b=d.scrollHeight;var e=d.offsetHeight;d.clientHeight!=e&&(b=c.scrollHeight,e=
c.offsetHeight);c=b>a?b>e?b:e:b<e?b:e}else c=0}this.f.style.height=c+"px";zg(this.f)}this.ka();c=Sh(this);Th(c);this.j=T(document,"keydown",r(this.eb,this));c=this.b;d=se("player-added",this.ka,this);we(c,"player-ready-pubsub-key",d);this.Ga&&(this.l=T(document,"click",r(this.qb,this)));zg(this.b);this.h.setAttribute("tabindex","0");Uh(this);this.B||C(document.body,"yt-dialog-active");ah($g.m());mh(Lh.m());mh(Mh.m());O("yt-ui-dialog-show-complete",this)}};
function Vh(){return Fa(Ac("yt-dialog"),function(a){return yg(a)})}
k.ka=function(){if(!this.Ja){var a=this.b;F(document.body,"hide-players",!0);a&&F(a,"preserve-players",!0)}};
function Sh(a){var b=Bc(document,"iframe",null,a.b);x(b,function(a){var b=ye(a,"onload");b&&(b=q(b))&&T(a,"load",b);if(b=ye(a,"src"))a.src=b},a);
return Ja(b)}
function Th(a){x(document.getElementsByTagName("iframe"),function(b){-1==Ca(a,b)&&C(b,"iframe-hid")})}
function Wh(){x(Ac("iframe-hid"),function(a){D(a,"iframe-hid")})}
k.kb=function(a){a=a.currentTarget;a.disabled||(a=ye(a,"action")||"",this.dismiss(a))};
k.dismiss=function(a){if(!this.L()){this.i.A("pre-all");this.i.A("pre-"+a);Ag(this.b);mh(Lh.m());mh(Mh.m());this.h.setAttribute("tabindex","-1");Vh()||(Ag(this.f),this.B||D(document.body,"yt-dialog-active"),Ue(),Wh());this.j&&(U(this.j),this.j=null);this.l&&(U(this.l),this.l=null);var b=this.b;if(b){var c=ye(b,"player-ready-pubsub-key");c&&(ue(c),ze(b,"player-ready-pubsub-key"))}this.i.A("post-all");O("yt-ui-dialog-hide-complete",this);"cancel"==a&&O("yt-ui-dialog-cancelled",this);this.i&&this.i.A("post-"+
a);this.o&&this.o.focus()}};
k.setTitle=function(a){Nc(I("yt-dialog-title",this.b),a)};
k.eb=function(a){M(r(function(){this.ub||27!=a.keyCode||this.dismiss("cancel")},this),0);
9==a.keyCode&&a.shiftKey&&B(document.activeElement,"yt-dialog-fg")&&a.preventDefault()};
k.qb=function(a){"yt-dialog-base"==a.target.className&&this.dismiss("cancel")};
k.L=function(){return this.za};
k.dispose=function(){yg(this.b)&&this.dismiss("dispose");U(this.w);this.w.length=0;M(r(function(){this.o=null},this),0);
this.P=this.h=null;this.i.dispose();this.i=null;this.za=!0};
k.Xa=function(a){a.stopPropagation();Uh(this)};
function Uh(a){M(r(function(){this.h&&this.h.focus()},a),0)}
t("yt.ui.Dialog",Oh);function Xh(){W.call(this,"overlay");this.i=this.f=this.h=this.b=null}
u(Xh,W);pa(Xh);k=Xh.prototype;k.register=function(){Y(this,"click",this.W,"target");Y(this,"click",this.hide,"close");Yh(this)};
k.unregister=function(){Xh.u.unregister.call(this);Z(this,"click",this.W,"target");Z(this,"click",this.hide,"close");this.i&&(ue(this.i),this.i=null);this.f&&(U(this.f),this.f=null)};
k.W=function(a){if(!this.b||!yg(this.b.b)){var b=this.v(a);a=Zh(b,a);b||(b=a?a.overlayParentNode:null);if(b&&a){var c=!!this.g(b,"disable-shortcuts")||!1,d=!!this.g(b,"disable-outside-click-dismiss")||!1;this.b=new Oh(a,c);this.h=b;var e=I("yt-dialog-fg",a);if(e){var f=this.g(b,"overlay-class")||"",g=this.g(b,"overlay-style")||"default",h=this.g(b,"overlay-shape")||"default";f=f?f.split(" "):[];f.push(X(this,g));f.push(X(this,h));bc(e,f)}this.b.show();O("yt-uix-kbd-nav-move-to",e||a);Yh(this);c||
d||(c=r(function(a){B(a.target,"yt-dialog-base")&&$h(this)},this),this.f=T(I("yt-dialog-base",a),"click",c));
this.G(b,"overlay-shown");O("yt-uix-overlay-shown",b)}}};
function Yh(a){a.i||(a.i=se("yt-uix-overlay-hide",ai));a.b&&Rh(a.b,function(){var a=Xh.m();a.h=null;a.b.dispose();a.b=null})}
function $h(a){if(a.b){var b=a.h;a.b.dismiss("overlayhide");b&&a.G(b,"overlay-hidden");a.h=null;a.f&&(U(a.f),a.f=null);a.b=null}}
function Zh(a,b){var c;if(a)if(c=I("yt-dialog",a)){var d=H("body-container");d&&(d.appendChild(c),a.overlayContentNode=c,c.overlayParentNode=a)}else c=a.overlayContentNode;else b&&(c=J(b,"yt-dialog"));return c}
function bi(){var a=Xh.m();if(a.h)a=I("yt-dialog-fg-content",a.h.overlayContentNode);else a:{if(a=Ac("yt-dialog-fg-content"))for(var b=0;b<a.length;b++){var c=J(a[b],"yt-dialog");if(yg(c)){a=a[b];break a}}a=null}return a}
k.hide=function(a){a&&a.disabled||O("yt-uix-overlay-hide")};
function ai(){$h(Xh.m())}
k.show=function(a){this.W(a)};var ci={},di=[];function ei(a){a=J(a,"yt-uix-button-subscription-container");return I("yt-dialog",I("unsubscribe-confirmation-overlay-container",a))}
function fi(a,b){U(di);di.length=0;ci[b]||(ci[b]=ei(a));Xh.m().show(ci[b]);var c=bi();return new Ed(function(a){di.push(af(c,function(){a()},"overlay-confirmation-unsubscribe-button"))})}
;function gi(){var a=L("PLAYER_CONFIG");return a&&a.args&&void 0!==a.args.authuser?!0:!(!L("SESSION_INDEX")&&!L("LOGGED_IN"))}
;function hi(){W.call(this,"subscription-button")}
u(hi,W);pa(hi);hi.prototype.register=function(){Y(this,"click",this.X);gf(this,Hf,this.ua);gf(this,If,this.ta);gf(this,Jf,this.nb);gf(this,Mf,this.ua);gf(this,Nf,this.ta);gf(this,Of,this.pb);gf(this,Qf,this.mb);gf(this,Rf,this.lb)};
hi.prototype.unregister=function(){Z(this,"click",this.X);hi.u.unregister.call(this)};
var ii={Y:"hover-enabled",Ha:"yt-uix-button-subscribe",Ia:"yt-uix-button-subscribed",wb:"ypc-enabled",Ka:"yt-uix-button-subscription-container",La:"yt-subscription-button-disabled-mask-container"},ji={xb:"channel-external-id",Ma:"subscriber-count-show-when-subscribed",Na:"subscriber-count-tooltip",Oa:"subscriber-count-title",zb:"href",Ab:"insecure",Z:"is-subscribed",Bb:"parent-url",Cb:"clicktracking",Pa:"show-unsub-confirm-dialog",Db:"show-unsub-confirm-time-frame",Qa:"style-type",aa:"subscribed-timestamp",
ba:"subscription-id",Eb:"target",Ra:"ypc-enabled"};k=hi.prototype;k.X=function(a){var b=this.g(a,"href"),c=this.g(a,"insecure");if(b)a=this.g(a,"target")||"_self",window.open(b,a);else if(!c)if(gi()){b=this.g(a,"channel-external-id");c=this.g(a,"clicktracking");var d=ki(this,a),e=this.g(a,"parent-url");if(this.g(a,"is-subscribed")){var f=this.g(a,"subscription-id"),g=new Df(b,f,d,a,c,e);li(this,a)?fi(a,b).then(function(){S(Lf,g)}):S(Lf,g)}else S(Gf,new Bf(b,d,c,e))}else mi(this,a)};
k.ua=function(a){this.F(a.b,this.va,!0)};
k.ta=function(a){this.F(a.b,this.va,!1)};
k.nb=function(a){this.F(a.b,this.wa,!0,a.f)};
k.pb=function(a){this.F(a.b,this.wa,!1)};
k.mb=function(a){this.F(a.b,this.Wa)};
k.lb=function(a){this.F(a.b,this.Va)};
k.wa=function(a,b,c){b?(we(a,ji.Z,"true"),c&&we(a,ji.ba,c),this.g(a,ji.Pa)&&(b=new wb,we(a,ji.aa,(b.getTime()/1E3).toString()))):(ze(a,ji.Z),ze(a,ji.aa),ze(a,ji.ba));ni(this,a)};
function ki(a,b){if(!a.g(b,"ypc-enabled"))return null;var c=a.g(b,"ypc-item-type"),d=a.g(b,"ypc-item-id");return{itemType:c,itemId:d,subscriptionElement:b}}
k.va=function(a,b){var c=J(a,ii.Ka);F(c,ii.La,b);a.setAttribute("aria-busy",b?"true":"false");a.disabled=b};
function ni(a,b){var c=a.g(b,ji.Qa),d=!!a.g(b,"is-subscribed");c="-"+c;var e=ii.Ia+c;F(b,ii.Ha+c,!d);F(b,e,d);a.g(b,ji.Na)&&!a.g(b,ji.Ma)&&(c=X(Bg.m()),F(b,c,!d),b.title=d?"":a.g(b,ji.Oa));d?M(function(){C(b,ii.Y)},1E3):D(b,ii.Y)}
k.Wa=function(a){var b=!!this.g(a,"ypc-item-type"),c=!!this.g(a,"ypc-item-id");!this.g(a,"ypc-enabled")&&b&&c&&(C(a,"ypc-enabled"),we(a,ji.Ra,"true"))};
k.Va=function(a){this.g(a,"ypc-enabled")&&(D(a,"ypc-enabled"),ze(a,"ypc-enabled"))};
function oi(a,b){return Da(Ac(X(a)),function(a){return b==this.g(a,"channel-external-id")},a)}
k.Ta=function(a,b,c){var d=Ma(arguments,2);x(a,function(a){b.apply(this,Ia(a,d))},this)};
k.F=function(a,b,c){var d=oi(this,a);this.Ta.apply(this,Ia([d],Ma(arguments,1)))};
function mi(a,b){var c=r(function(a){a.discoverable_subscriptions&&le("SUBSCRIBE_EMBED_DISCOVERABLE_SUBSCRIPTIONS",a.discoverable_subscriptions);this.X(b)},a);
Jg(c)}
function li(a,b){if(!a.g(b,"show-unsub-confirm-dialog"))return!1;var c=a.g(b,"show-unsub-confirm-time-frame");return"always"==c||"ten_minutes"==c&&(c=parseInt(a.g(b,"subscribed-timestamp"),10),(new wb).getTime()<1E3*(c+600))?!0:!1}
;fg=!0;gg.push(Me(Gf,hg),Me(Lf,jg));fg||gg.push(Me(Kf,hg),Me(Pf,jg),Me(Ef,lg),Me(Ff,ng),Me(Sf,pg),Me(bg,rg),Me(dg,tg),Me(ag,sg));var pi=hi.m(),qi=X(pi);qi in jf||(pi.register(),ff(pi,"yt-uix-init-"+qi,pi.init),ff(pi,"yt-uix-dispose-"+qi,pi.dispose),jf[qi]=pi);t("yt.setConfig",le);t("yt.config.set",le);}).call(this);
