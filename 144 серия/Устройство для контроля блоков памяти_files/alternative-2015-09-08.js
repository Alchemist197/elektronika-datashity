<!--
function getSelectionHtml() {
    var A = '';
    if (typeof window.getSelection != 'undefined') {
        var B = window.getSelection();
        if (B.rangeCount) {
            var C = document.createElement('div');
            for (var i = 0, len = B.rangeCount; i < len; ++i) {
                C.appendChild(B.getRangeAt(i).cloneContents());
            };
            A = C.innerHTML;
        }
    } else if (typeof document.selection != 'undefined') {
        if (document.selection.type == 'Text') {
            A = document.selection.createRange().htmlText;
        }
    };
    return A;
};
function addLinkToClipboard() {
    var A, selected_text;
    if (window.getSelection) { A = window.getSelection(); } else { A = document.selection.createRange();    };
    selected_text = getSelectionHtml();
    if (selected_text.length < 128) return true;
    var B = '<br/><br/><a href="' + document.location.href + '">' + document.location.href + '</a><br/>&copy; ';
    if (location.hostname == 'www.findpatent.ru') {
	B += 'FindPatent.ru - патентный поиск';
    }
    if (location.hostname == 'findpatent.com.ua') {
	B += 'FindPatent.com.ua - патенти на винаходи';
    }
    if (location.hostname == 'russianpatents.com') {
	B += 'RussianPatents.com - patent search';
    }
    B += ', 2012-' + (new Date()).getFullYear();
    var C = document.createElement('div');
//    C.style.position = 'absolute';
//    C.style.left = '-9999px';
//    C.style.top = '0px';
    C.innerHTML = selected_text + B;
    var D = document.getElementsByTagName('body')[0];
    D.appendChild(C);
    if (window.getSelection) {
        A.selectAllChildren(C);
    } else {
        A.moveToElementText(C);
        A.select();
    };
    window.setTimeout(function () {
        D.removeChild(C);
    }, 0);
};
String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, '');
};
Array.prototype.contains = function (A) {
    for (var i = this.length - 1; i >= 0; --i) if (this[i] === A) return i;
    return -1;
};
if (typeof(Array.prototype.indexOf) == 'undefined') {
    Array.prototype.indexOf = function (A) {
        var B = this.length;
        var C = Number(arguments[1]) || 0;
        C = (C < 0) ? Math.ceil(C) : Math.floor(C);
        if (C < 0) C += B;
        for (; C < B; ++C) {
            if (C in this && this[C] === A) return C;
        };
        return -1;
    };
};
var Cons = {
    isIE: /*@cc_on!@*/
    false,
    isGecko: navigator.userAgent.toLowerCase().indexOf('gecko') != -1,
    browser: {},
    gel: function (a) {
        return document.getElementById ? document.getElementById(a) : (document.all ? document.all[a] : null);
    },
    gelByName: function (A, B) {
        var C = [];
        var D = document.getElementsByTagName(A);
        var E = D.length;
        for (var i = 0, attr; i < E; i++) {
            attr = D[i].getAttribute('name');
            if (attr == B) C.push(D[i]);
        };
        return C;
    },
    appl: function (a, b) {
        return function () {
            return a[b].apply(a, arguments);
        }
    },
    isAlpha: function (A) {
        return (A > 255) || (A >= 65 && A <= 122) || (A >= 192 && A <= 255) || (A >= 48 && A <= 57);
    },
    _escapeOrig: window.escape,
    _escape_trans: [],
    _charset: (document.characterSet ? document.characterSet : document.charset).toLowerCase(),
    escape: function (A) {
        if (this._charset == 'utf-8') return A;
        if (this._escape_trans.length == 0) {
            for (var i = 0x410; i <= 0x44F; i++) this._escape_trans[i] = i - 0x350;
            this._escape_trans[0x401] = 0xA8;
            this._escape_trans[0x451] = 0xB8;
        };
        var B = [];
        var n;
        for (var i = 0; i < A.length; i++) {
            n = A.charCodeAt(i);
            if (typeof(this._escape_trans[n]) != 'undefined') n = this._escape_trans[n];
            if (n <= 0xFF) B.push(n);
        };
        return this._escapeOrig(String.fromCharCode.apply(null, B));
    },
    init: function () {
        var A = navigator.userAgent.toLowerCase();
        var m = /(webkit)[ \/]([\w.]+)/.exec(A) || /(opera)(?:.*version)?[ \/]([\w.]+)/.exec(A) || /(msie) ([\w.]+)/.exec(A) || !/compatible/.test(A) && /(mozilla)(?:.*? rv:([\w.]+))?/.exec(A) || [];
        this.browser = {
            family: m[1] || '',
            version: m[2] || 0,
            mobile: false
        };
        this.browser.mobile = function () {
            var a = navigator.userAgent || navigator.vendor || window.opera;
            return (/android|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))
        }();
        this.platform = '';
        if ((A.search("android") > -1) || (A.search("googletv") > -1) || (A.search("htc_flyer") > -1)) {
            this.platform = 'Android';
        } else if (A.search("iphone") > -1 || A.search("ipod") > -1 || A.search("ipad") > -1) {
            this.platform = 'iOS';
        } else if (A.search("windows phone os 7") > -1 || A.search("windows phone 8") > -1) {
            this.platform = 'WindowsPhone';
        } else if (A.search("windows ce") > -1 || A.search("iemobile") > -1 || A.search("wm5 pie") > -1 || (A.search("htc") > -1 && A.search("windows") > -1) || (A.search("ppc") > -1 && A.search("macintosh") == -1)) {
            this.platform = 'WindowsMobile';
        }
    },
    isWindowsMobile: function () {
        return this.platform == 'WindowsMobile';
    },
    isWindowsPhone: function () {
        return this.platform == 'WindowsPhone';
    },
    isAndroid: function () {
        return this.platform == 'Android';
    },
    isIOS: function () {
        return this.platform == 'iOS';
    },
    innerHeight: function () {
        var r = 0;
        if (typeof(window.innerHeight) == 'number') r = window.innerHeight;
        else if (document.documentElement && document.documentElement.clientHeight) r = document.documentElement.clientHeight;
        else if (document.body && document.body.clientHeight) r = document.body.clientHeight;
        return r;
    },
    innerWidth: function () {
        var r = 0;
        if (typeof(window.innerWidth) == 'number') r = window.innerWidth;
        else if (document.documentElement && document.documentElement.clientWidth) r = document.documentElement.clientWidth;
        else if (document.body && document.body.clientWidth) r = document.body.clientWidth;
        return r;
    },
    ver: '0.1'
};
Cons.init();
Cons.Event = {
    add: function (A, B, C, D) {
        if (!A) return;
        if (typeof(D) == 'undefined') D = false;
        if (A.addEventListener) A.addEventListener(B, C, D);
        else if (A.attachEvent) {
            A['e' + B + C] = C;
            A[B + C] = function () {
                A['e' + B + C](window.event);
            };
            A.attachEvent('on' + B, A[B + C]);
            window.attachEvent('onunload', function () {
                A.detachEvent('on' + B, C);
            });
        } else A['on' + B] = C;
    },
    remove: function (A, B, C) {
        if (!A) return;
        if (A.removeEventListener) A.removeEventListener(B, C, false);
        else if (A.detachEvent) {
            A.detachEvent('on' + B, A[B + C]);
            A[B + C] = null;
            A['e' + B + C] = null;
        } else A['on' + B] = null;
    },
    format: function (A) {
        if (Cons.isIE) {
            A.charCode = (A.type == 'keypress') ? A.keyCode : 0;
            A.eventPhase = 2;
            A.isChar = (A.charCode > 0);
            A.pageX = A.clientX + document.body.scrollLeft;
            A.pageY = A.clientY + document.body.scrollTop;
            A.preventDefault = function () {
                this.returnValue = false;
            };
            if (A.type == 'mouseout') A.relatedTarget = A.toElement;
            else if (A.type == 'mouseover') A.relatedTarget = A.fromElement;
            A.stopPropagation = function () {
                this.cancelBubble = true;
            };
            A.target = A.srcElement;
            A.time = (new Date).getTime();
        };
        return A;
    }
};
Cons.Event.add(window, 'load', function () {
	Cons.Event.add(Cons.gel('content'), 'copy', addLinkToClipboard);
});
-->