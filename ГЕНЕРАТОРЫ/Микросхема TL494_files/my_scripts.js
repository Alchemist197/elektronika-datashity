//dinamic PIE for IE 7-8
var	ie = jQuery.browser.msie,
	ieV = jQuery.browser.version,
	ltie7 = ie&&(ieV <= 7),
	ltie8 = ie&&(ieV <= 8);

	function setPie(selectors){jQuery(selectors).css("behavior", "url(PIE.htc)")};
	function unsetPie(selectors){jQuery(selectors).css("behavior", "none")};
	function resetPie(selectors){
	unsetPie(selectors);
	setPie(selectors);
	};
		
jQuery(document).ready(function(){	

	//dinamic PIE width hover	
	jQuery(".menu_top li").hover(
	  function () {
		ltie8 ? resetPie(".menu_top li") : false;	
	  });
	  
	  
//slider
	 $('.slider2').mobilyslider({
        transition: 'vertical',
        animationSpeed: 500,
        autoplay: true,
        autoplaySpeed: 3000,
        pauseOnHover: true,
        bullets: false
    });