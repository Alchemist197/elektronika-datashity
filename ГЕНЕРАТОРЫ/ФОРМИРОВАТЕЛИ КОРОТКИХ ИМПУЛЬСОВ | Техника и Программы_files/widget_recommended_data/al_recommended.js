WRecommended = {

  init: function (options) {
    extend(cur, {
      options: options,
      heightEl: ge('wrecommended_page'),
      moreEl: geByClass1('_wrecommended_more'),
      contentEl: geByClass1('_wrecommended_content'),
      noAwayCheck: true
    });

    var resizeWidget = this.resizeWidget.bind(this);
    cur.RpcMethods = {
      onInit: function() {
        setTimeout(resizeWidget, 0);
        setTimeout(resizeWidget, 500);
      }
    };

    try {
      cur.Rpc = new fastXDM.Client(cur.RpcMethods, {safe: true});
      cur.resizeInt = setInterval(resizeWidget, 1000);
    } catch (e) {
      debugLog(e);
    }

    addEvent(cur.moreEl, 'click', this.showMore.bind(this));
  },

  resizeWidget: function () {
    if (cur.heightEl && cur.Rpc) {
      var size = getSize(cur.heightEl)[1];
      if (browser.msie && !browser.msie8 || browser.opera) size += 15;
      cur.Rpc.callMethod('resize', size);
    }
  },

  showMore: function () {
    if (buttonLocked(cur.moreEl)) return;
    ajax.post('widget_recommended.php', cur.options.show_more_params, {
      onDone: function (rows) {
        val(cur.contentEl, rows);
        hide(cur.moreEl);
        this.resizeWidget();
      }.bind(this),
      showProgress: lockButton.pbind(cur.moreEl),
      hideProgress: unlockButton.pbind(cur.moreEl)
    });
  }

};

try{stManager.done('api/widgets/al_recommended.js');}catch(e){}
