// SiNG Content Management System
// Copyright � 2009-2014 Sergei Gushchin http://sing-cms.ru
// file jscript.js version 140702

function inserttext(txtarea, what) {
	if (document.forms['inputform'].elements[txtarea].createTextRange) {
		document.forms['inputform'].elements[txtarea].focus();
		document.selection.createRange().duplicate().text = what;
	}
	else if ((typeof document.forms['inputform'].elements[txtarea].selectionStart) != 'undefined') {
		var tarea = document.forms['inputform'].elements[txtarea], selEnd = tarea.selectionEnd, txtLen = tarea.value.length, txtbefore = tarea.value.substring(0,selEnd), txtafter =  tarea.value.substring(selEnd, txtLen), oldScrollTop = tarea.scrollTop;
		tarea.value = txtbefore + what + txtafter;
		tarea.selectionStart = txtbefore.length + what.length;
		tarea.selectionEnd = txtbefore.length + what.length;
		tarea.scrollTop = oldScrollTop;
		tarea.focus();
	}
	else {
		document.forms['inputform'].elements[txtarea].value += what;
		document.forms['inputform'].elements[txtarea].focus();
	}
}
function addtext(txtarea, wrap1, wrap2) {
	if (document.selection) {
		var str = document.selection.createRange().text;
		document.forms['inputform'].elements[txtarea].focus();
		var sel = document.selection.createRange();
		sel.text = wrap1 + str + wrap2;
		return;
	}
	else if ((typeof document.forms['inputform'].elements[txtarea].selectionStart) != 'undefined') {
		var txarea = document.forms['inputform'].elements[txtarea], selLength = txarea.textLength, selStart = txarea.selectionStart, selEnd = txarea.selectionEnd, oldScrollTop = txarea.scrollTop, s1 = (txarea.value).substring(0, selStart);
		if (document.getSelection() != '') var s2 = document.getSelection();
		else var s2 = (txarea.value).substring(selStart, selEnd);
		var s3 = (txarea.value).substring(selEnd, selLength);
		txarea.value = s1 + wrap1 + s2 + wrap2 + s3;
		txarea.selectionStart = s1.length;
		txarea.selectionEnd = s1.length + s2.length + wrap1.length + wrap2.length;
		txarea.scrollTop = oldScrollTop;
		txarea.focus();
		return;
	}
	else inserttext(wrap1 + wrap2);
}
function DeleteItem($item) {
	return confirm($item);
}
function textCounter(field, maxlimit) {
	if (field.value.length > maxlimit) field.value = field.value.substring(0, maxlimit);
}
function addURL(txtarea, wrap1, wrap2, wrap3, lng1, lng2, lng3){
	var enterURL, enterTITLE, wrap;
	if (document.selection) {
		enterTITLE = document.selection.createRange().text;
		if (enterTITLE.substring(0, 4) == 'http') {
			if (wrap1 == '[url=') wrap = '[url' + wrap2;
			else {
				enterTITLE = enterTITLE.split('&').join('&amp;');
				wrap = wrap1 + enterTITLE + wrap2;
			}
		}
		else {
			enterURL  = prompt(lng1, "http://");
			if (enterTITLE.length == 0) enterTITLE = prompt(lng2, lng3); 
			if (!enterURL || enterURL == 'http://' || !enterTITLE) return;
			if (wrap1 != '[url=') enterURL = enterURL.split('&').join('&amp;');
			wrap = wrap1 + enterURL + wrap2;
		}
		document.forms['inputform'].elements[txtarea].focus();
		var sel = document.selection.createRange();
		sel.text = wrap + enterTITLE + wrap3;
		return;
	}
	else if ((typeof document.forms['inputform'].elements[txtarea].selectionStart) != 'undefined') {
		var txarea = document.forms['inputform'].elements[txtarea], selLength = txarea.textLength, selStart = txarea.selectionStart, selEnd = txarea.selectionEnd, oldScrollTop = txarea.scrollTop, s1 = (txarea.value).substring(0, selStart);
		enterTITLE = (txarea.value).substring(selStart, selEnd);
		if (enterTITLE.substring(0, 4) == 'http') {
			if (wrap1 == '[url=') wrap = '[url' + wrap2;
			else {
				enterTITLE = enterTITLE.split('&').join('&amp;');
				wrap = wrap1 + enterTITLE + wrap2;
			}
		}
		else {
			enterURL  = prompt(lng1, 'http://');
			if (enterTITLE.length == 0) enterTITLE = prompt(lng2, lng3); 
			if (!enterURL || enterURL == 'http://' || !enterTITLE) return;
			if (wrap1 != '[url=') enterURL = enterURL.split('&').join('&amp;');
			wrap = wrap1 + enterURL + wrap2;
		}
		var s3 = (txarea.value).substring(selEnd, selLength);
		txarea.value = s1 + wrap + enterTITLE + wrap3 + s3;
		txarea.selectionStart = s1.length;
		txarea.selectionEnd = s1.length + enterTITLE.length + wrap.length + wrap3.length;
		txarea.scrollTop = oldScrollTop;
		txarea.focus();
		return;
	} 
}
function setCookie(name, value, expiredays) {
	if (expiredays) {
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + expiredays);
		var expires = exdate.toGMTString();
	}
	document.cookie = name + '=' + escape(value) + (expiredays ? '; expires=' + expires : '') + '; path=/; domain=.' + (window.location.hostname).replace(/^www\./g, '');
	document.cookie = name + '_=' + escape(value) + (expiredays ? '; expires=' + expires : '') + '; path=/';
}
function delCookie(name) {
	setCookie(name, '', -1);
}
function getCookie(name) {
	var cookie = ' ' + document.cookie, search = ' ' + name + '=', setStr = null, offset = 0, end;
	if (cookie.length > 0) {
		offset = cookie.indexOf(search);
		if (offset != -1) {
			offset += search.length;
			end = cookie.indexOf(';', offset);
			if (end == -1) end = cookie.length;
			setStr = unescape(cookie.substring(offset, end));
		}
		else {
			search = ' ' + name + '_=';
			offset = cookie.indexOf(search);
			if (offset != -1) {
				offset += search.length;
				end = cookie.indexOf(';', offset);
				if (end == -1) end = cookie.length;
				setStr = unescape(cookie.substring(offset, end));
			}
		}
	}
	return(setStr);
}
function showTip(val) {
	var el = document.createElement('div'), x, y;
	el.setAttribute('id', 'tip');
	document.body.appendChild(el);
	document.onmouseover = document.onmousemove = moveTip;
	function moveTip(e) {
		if (document.all) { x = event.x + document.body.scrollLeft; y = event.y + document.body.scrollTop; }
		else { x = e.pageX; y = e.pageY; }
		el.style.left = x + 'px';
		el.style.top = y + 20 + 'px';
	}
	el.innerHTML = '<div class="tooltip">' + val + '</div>';
	el.style.position = 'absolute';
}
function hideTip() {
	var el = document.getElementById('tip');
	document.body.removeChild(el); 
}
function showSmilies(basedir, txtarea, id, nums) {
	var el = document.getElementById(id), qwe = [], snums = nums.split(',');
	el.innerHTML = '';
	for (var i = 0; i < snums.length; i++) {
		qwe[i] = new Image();
		qwe[i].src = basedir + 'images/smilies/sm' + snums[i] + '.gif'; // IE load smilies
		el.innerHTML += '<img src="' + basedir + 'images/smilies/sm' + snums[i] + '.gif" alt="" title="sm' + snums[i] + '" onclick="inserttext(\'' + txtarea + '\', \':sm' + snums[i] + ':\')"> ';
	}
}
function codeWidth() {
	var ie = document.all ? true : false;
	var els = document.getElementsByTagName('code');
	for (var i = 0; i < els.length; i++) {
		var x = els[i].parentNode.offsetWidth;
		els[i].style.width = x - 30 + 'px';
		if (ie) {
			var y = els[i].offsetHeight;
			if (y - els[i].clientHeight > 6) els[i].style.height = y + 16 + 'px';
			if (y > 500) els[i].style.height = 500 + 'px';
		}
	}
}